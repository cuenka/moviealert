var gulp = require('gulp'),
    gp_concat = require('gulp-concat'),
    gp_rename = require('gulp-rename'),
    gp_sass = require('gulp-sass'),
    gp_minify = require('gulp-clean-css'),
    gp_watch = require('gulp-watch'),
    gp_uglify = require('gulp-uglify');

gulp.task('default', ['js', 'css', 'webFonts', 'images'], function () {
});
gulp.task('defaultLite', ['js', 'css'], function () {
});
var jsSource =
    [
        'node_modules/jquery/dist/jquery.min.js',
        'node_modules/jquery-ui-npm/jquery-ui.js',
        'node_modules/popper.js/dist/umd/popper.js',
        'node_modules/bootstrap/dist/js/bootstrap.min.js',
        'node_modules/select2/dist/js/select2.min.js',
        'node_modules/slick-carousel/slick/slick.js',
        'node_modules/modal-video/js/jquery-modal-video.js',
        'js/*.js'
    ];

var scssSource =
    [
        'node_modules/jquery-ui-npm/jquery-ui.css',
        'node_modules/font-awesome/scss/font-awesome.scss',
        'node_modules/bootstrap/dist/css/bootstrap.css',
        'node_modules/select2/dist/css/select2.min.css',
        'node_modules/slick-carousel/slick/slick.scss',
        'node_modules/modal-video/scss/modal-video.scss',
        'scss/universal.scss'
    ];
var scssSourceListening =
    [
        'scss/*.scss'
    ];
var fontSource =
    [
        'components/font-awesome/fonts/*'
    ];
var imagesSource =
    [
        'components/jquery-ui/themes/base/images/*'
    ];

var finalJsPath = '../../web/js';
var finalCssPath = '../../web/css';
var finalFontPath = '../../web/fonts';
var finalImagesPath = '../../web/css/images';

gulp.task('watch', function () {
    gulp.watch([scssSourceListening, jsSource], ['defaultLite']);
});

gulp.task('js', function () {
    return gulp.src(jsSource)
        .pipe(gp_concat('final.js'))
        .pipe(gulp.dest(finalJsPath))
        .pipe(gp_rename('final.js'))
        // .pipe(gp_uglify())
        .pipe(gulp.dest(finalJsPath));
});

gulp.task('css', function () {
    return gulp.src(scssSource)
        .pipe(gp_sass())
        .pipe(gp_concat('scss-files.scss'))
        .pipe(gp_minify(({debug: true}, function (details) {
            console.log(details.name + ': ' + details.stats.originalSize);
            console.log(details.name + ': ' + details.stats.minifiedSize);
        })))
        .pipe(gp_rename('final.css'))
        .pipe(gulp.dest(finalCssPath))
});

gulp.task('webFonts', function () {
    return gulp.src(fontSource)
        .pipe(gulp.dest(finalFontPath))
});

gulp.task('images', function () {
    return gulp.src(imagesSource)
        .pipe(gulp.dest(finalImagesPath))
});