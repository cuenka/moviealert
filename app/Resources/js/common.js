$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $(".js-modal-btn").modalVideo();
    $(".score-number").each(function () {
        var score = $(this);
        var decimals = parseInt(score.attr("data-decimals"));
        $({
            Counter: 0
        }).animate({
            Counter: score.attr("data-stop")
        }, {
            duration: 2000,
            easing: "swing",
            step: function (a) {
                if (decimals == 0) {
                    score.text(parseInt(a));
                } else {
                    score.text(parseFloat(a).toFixed(decimals));
                }
            }
        })
    })
});