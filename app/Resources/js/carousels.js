$('.c-carousel-slick-large').slick({
    dots: false,
    autoplay: false,
    infinite: true,
    speed: 300,
    centerMode: false,
    arrows: true,
    variableWidth: false,
    slidesToShow: 5,
    slidesToScroll: 3,
    responsive: [
        {
            breakpoint: 1024,
            centerMode: true,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 2
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 480,
            centerMode: true,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});

$('.c-carousel-slick-medium').slick({
    dots: false,
    autoplay: false,
    infinite: true,
    speed: 300,
    centerMode: false,
    arrows: true,
    variableWidth: false,
    slidesToShow: 4,
    slidesToScroll: 2,
    responsive: [
        {
            breakpoint: 1024,
            centerMode: true,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 2
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        },
        {
            breakpoint: 480,
            centerMode: true,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});


$('.c-carousel-slick-small').slick({
    dots: false,
    infinite: true,
    speed: 300,
    centerMode: false,
    arrows: true,
    variableWidth: false,
    slidesToShow: 2,
    slidesToScroll: 1
});
