<?php

namespace AdministratorBundle\Controller;

use MovieBundle\Entity\Movie;
use MovieBundle\Form\MovieType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/movie")
 */
class MovieController extends Controller
{
    /**
     * @Route("/list", name="backend_movielist")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     */
    public function ListAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $movies = $em->getRepository('MovieBundle:Movie')
            ->getInfoForTable();

        $actionNames =
            [
              'backend_movieedit',
              'backend_moviedelete'
            ];
        return $this->render('AdministratorBundle:Movies:list.html.twig', array(
            'data' => $movies,
            'actions' => $actionNames,
        ));
    }

    /**
     * @Route("/{id}/delete", name="backend_moviedelete")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     */
    public function deleteAction(Request $request, Movie $movie)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($movie);
        $em->flush();
        return $this->redirectToRoute('backend_movielist');
    }

    /**
     * @Route("/{id}/edit", name="backend_movieedit")
     * @param Request $request
     * @param Movie $id
     * @return \Symfony\Component\HttpFoundation\Response
     *
     */
    public function EditAction(Request $request, Movie $movie)
    {
        $editForm = $this->createForm(MovieType::class, $movie);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('backend_movieedit', array('id' => $movie->getId()));
        }

        return $this->render('AdministratorBundle:Movies:edit.html.twig', array(
            'movie' => $movie,
            'form' => $editForm->createView()
        ));

        $em = $this->getDoctrine()->getManager();

        $movies = $em->getRepository('MovieBundle:Movie')
            ->getInfoForTable();

        $actionNames =
            [
                'backend_movieedit',
                'backend_moviedelete'
            ];
        return $this->render('AdministratorBundle:Movies:list.html.twig', array(
            'data' => $movies,
            'actions' => $actionNames,
        ));
    }
}
