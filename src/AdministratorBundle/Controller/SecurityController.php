<?php

namespace AdministratorBundle\Controller;

use AdministratorBundle\Entity\User;
use AdministratorBundle\Form\LoginType;
use AdministratorBundle\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Class SecurityController
 * @package AdministratorBundle\Controller
 */
class SecurityController extends Controller
{
    /**
     * @Route("/login", name="security_login")
     */
    public function loginAction(Request $request)
    {

        $authUtils = $this->get('security.authentication_utils');
        // get the login error if there is one
        $error = $authUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authUtils->getLastUsername();

        return $this->render('AdministratorBundle:Security:login.html.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error,
        ));
    }

    /**
     * @Route("/login-check", name="security_login-check")
     */
    public function loginCheckAction(Request $request)
    {
    }

    /**
     * @Route("/login-fail", name="security_login-fail")
     */
    public function loginFailAction(Request $request)
    {
        return $this->redirectToRoute('dashboard',[]);
    }

    /**
     * @Route("/logout", name="security_logout")
     */
    public function logoutAction()
    {

    }

    /**
     * @Route("/register", name="security_user-registration")
     */
    public function registerAction(Request $request)
    {
        // 1) build the form
        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $encoder = $this->get('security.password_encoder');
            $password = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($password);
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            return $this->redirectToRoute('security_login',[]);
        }
        return $this->render(
            'AdministratorBundle:Security:register.html.twig',
            array('form' => $form->createView())
        );
    }


}