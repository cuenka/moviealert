<?php

namespace MovieBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Cast
 *
 * @ORM\Table(name="keywords", indexes={
 * @ORM\Index(name="id_idx", columns={"id"}),
 * @ORM\Index(name="slug_idx", columns={"slug"})
 * })
 * @ORM\Entity(repositoryClass="MovieBundle\Repository\KeywordRepository")
 */
class Keyword
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    * @var \DateTime
    *
    * @ORM\Column(name="published", type="datetime")
    */
    private $published;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime")
     */
    private $modified;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, unique=true)
     */
    private $slug;

    /**
     * Many Keywords belongs to many movies
     * @ORM\ManyToMany(targetEntity="Movie", mappedBy="keyword")
     */
    private $film;

    /**
     * Keyword constructor.
     */
    public function __construct()
    {
        $this->film = new ArrayCollection();
        $this->published = new \DateTime();
        $this->modified = new \DateTime();
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param $slug
     * @return $this
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFilm()
    {
        return $this->film;
    }

    /**
     * @param mixed $film
     */
    public function setFilms(ArrayCollection $films)
    {
        $this->film = $films;

        return $this;
    }

    /**
     * @param Movie $movie
     * @return $this
     */
    public function addFilm(Movie $movie)
    {
        $this->film[] = $movie;

        return $this;
    }

}