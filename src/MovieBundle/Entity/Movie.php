<?php

namespace MovieBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Imdb\Title;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Movie
 *
 * @ORM\Table(name="movies", indexes={
 * @ORM\Index(name="movie_id_idx", columns={"id"}),
 * @ORM\Index(name="movie_slug_idx", columns={"slug"})
 * })
 * @ORM\Entity(repositoryClass="MovieBundle\Repository\MovieRepository")
 */
class Movie
{
    const SCREENSHOT_PATH = DIRECTORY_SEPARATOR. 'images'. DIRECTORY_SEPARATOR. 'screenshot'.
                            DIRECTORY_SEPARATOR. 'movie'. DIRECTORY_SEPARATOR;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="published", type="datetime")
     */
    private $published;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, unique=true)
     */
    private $slug;

    /**
     * @var int
     *
     * @ORM\Column(name="duration", type="integer", nullable=true)
     */
    private $duration;


    /**
     * @var string
     *
     * @ORM\Column(name="imdbId", type="string", length=50, unique=true)
     */
    private $imdbId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="needReview", type="boolean")
     */
    private $needReview = true;

    /**
     * @var boolean
     *
     * @ORM\Column(name="poster", type="boolean")
     */
    private $poster = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="screenshot", type="boolean")
     */
    private $screenshot = false;


    /**
     * @var text
     *
     * @ORM\Column(name="awards", type="text", nullable=true)
     */
    private $awards;

    /**
     * @var text
     *
     * @ORM\Column(name="plot", type="text", nullable=true)
     */
    private $plot;

    /**
     * @var text
     *
     * @ORM\Column(name="storyline", type="text", nullable=true)
     */
    private $storyline;

    /**
     * @var array
     *
     * @ORM\Column(name="releaseInfo", type="array", nullable=true)
     */
    private $releaseInfo;

    /**
     * @var string
     *
     * @ORM\Column(name="trailer", type="string", length=50, nullable=true)
     */
    private $trailer;

    /**
     * @var string
     *
     * @ORM\Column(name="rating", type="string", length=50, nullable=true)
     */
    private $rating;

    /**
     * @var float
     *
     * @ORM\Column(name="score", type="float", nullable=true)
     */
    private $score;

    /**
     * @var float
     *
     * @ORM\Column(name="imdbScore", type="float", nullable=true)
     */
    private $imdbScore;

    /**
     * @var integer
     *
     * @ORM\Column(name="rottenTomatoesScore", type="integer", nullable=true)
     */
    private $rottenTomatoesScore;

    /**
     * @var integer
     *
     * @ORM\Column(name="metacriticScore", type="integer", nullable=true)
     */
    private $metacriticScore;

    /**
     * @var integer
     *
     * @ORM\Column(name="budget", type="bigint", nullable=true)
     */
    private $budget;

    /**
     * @var integer
     *
     * @ORM\Column(name="revenue", type="bigint", nullable=true)
     */
    private $revenue;

    /**
     * @var integer
     *
     * @ORM\Column(name="year", type="integer", nullable=true)
     */
    private $year;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="released", type="datetime", nullable=true)
     */
    private $released;

    /**
     * Many movies have at least one language
     * @ORM\ManyToMany(targetEntity="Language", inversedBy="film")
     * @ORM\JoinTable(name="movie_language",)
     */
    private $language;

    /**
     * One movie have Many characters.
     * @ORM\OneToMany(targetEntity="Character", mappedBy="movie", cascade={"remove", "persist"})
     */
    private $character;

    /**
     * Many Movies have Many Directors.
     * @ORM\ManyToMany(targetEntity="Director", inversedBy="filmography", cascade={"remove", "persist"})
     * @ORM\JoinTable(name="movie_director")
     */
    private $director;

    /**
     *Many Movies have many composers
     * @ORM\ManyToMany(targetEntity="Composer", inversedBy="filmography")
     * @ORM\JoinTable(name="movie_composer")
     */
    private $composer;

    /**
     * Many movies have Many producers.
     * @ORM\ManyToMany(targetEntity="Producer", inversedBy="filmography")
     * @ORM\JoinTable(name="movie_producer")
     */
    private $producer;

    /**
     * Many movies have at least one writer
     * @ORM\ManyToMany(targetEntity="Writer", inversedBy="filmography")
     * @ORM\JoinTable(name="movie_writer")
     */
    private $writer;

    /**
     * Many Users have Many Groups.
     * @ORM\ManyToMany(targetEntity="Production", inversedBy="films")
     * @ORM\JoinTable(name="movie_production")
     */
    private $production;

    /**
     * Many movies have at least one genre
     * @ORM\ManyToMany(targetEntity="Genre", inversedBy="film")
     * @ORM\JoinTable(name="movie_genre")
     */
    private $genre;

    /**
     * Many movies have at least one Keyword
     * @ORM\ManyToMany(targetEntity="Keyword", inversedBy="film")
     * @ORM\JoinTable(name="movie_keyword")
     */
    private $keyword;

    /**
     * @var array
     *
     * @ORM\ManyToMany(targetEntity="Country", inversedBy="film")
     * @ORM\JoinTable(name="movie_country")
     */
    private $country;


    /**
     * Movie constructor.
     * @param int $id
     */
    public function __construct()
    {
        $this->published = new \DateTime();
        $this->updated = new \DateTime();
        $this->released = null;
        $this->director = new ArrayCollection();
        $this->character = new ArrayCollection();
        $this->composer = new ArrayCollection();
        $this->country = new ArrayCollection();
        $this->genre = new ArrayCollection();
        $this->writer = new ArrayCollection();
        $this->language = new ArrayCollection();
        $this->production = new ArrayCollection();
        $this->producer = new ArrayCollection();
        $this->needReview = true;
        $this->screenshot = false;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set published
     *
     * @param \DateTime $published
     *
     * @return Movie
     */
    public function setPublished($published)
    {
        $this->published = $published;

        return $this;
    }

    /**
     * Get published
     *
     * @return \DateTime
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return Movie
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Movie
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return text
     */
    public function getPlot()
    {
        return $this->plot;
    }

    /**
     * @param text $plot
     */
    public function setPlot($plot)
    {
        $this->plot = $plot;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Movie
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set language
     *
     * @param ArrayCollection $language
     *
     * @return Movie
     */
    public function setLanguage(ArrayCollection $language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * @param Language $language
     * @return $this
     */
    public function addLanguage(Language $language)
    {
        $this->language[] = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }


    /**
     * Set director
     *
     * @param ArrayCollection $director
     *
     * @return Movie
     */
    public function setDirector($director)
    {
        $this->director = $director;

        return $this;
    }

    /**
     * Get director
     *
     * @return array
     */
    public function getDirector()
    {
        return $this->director;
    }

    /**
     * @param Director $director
     * @return $this
     */
    public function addDirector(Director $director)
    {
        $this->director[] = $director;

        return $this;
    }

    /**
     * Check if Genre already exist on Relationship, avoid duplicate entries
     * @param Genre $potentialGenre
     * @return boolean
     */
    public function checkGenreAdded(Genre $potentialGenre)
    {
        foreach ($this->getGenre() as $genre) {
            if ($genre->getId() == $potentialGenre->getId()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if Keyword already exist on Relationship, avoid duplicate entries
     * @param Keyword $potentialKeyword
     * @return boolean
     */
    public function checkKeywordAdded(Keyword $potentialKeyword)
    {
        if (!is_null($this->getKeyword())) {
            foreach ($this->getKeyword() as $keyword) {
                if ($keyword->getId() == $potentialKeyword->getId()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Check if country already exist on Relationship, avoid duplicate entries
     * @param Country $potentialCountry
     * @return boolean
     */
    public function checkCountryAdded(Country $potentialCountry)
    {
        foreach ($this->getCountry() as $country) {
            if ($country->getIsoCode() == $potentialCountry->getIsoCode()) {
                return true;
            }
        }

        return false;
    }


    /**
     * Check if Language already exist on Relationship, avoid duplicate entries
     * @param Language $potentialLanguage
     * @return boolean
     */
    public function checkLanguageAdded(Language $potentialLanguage)
    {
        foreach ($this->getLanguage() as $Language) {
            if ($Language->getId() == $potentialLanguage->getId()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if director already exist on Relationship, avoid duplicate entries
     * @param Director $potentialDirector
     * @return boolean
     */
    public function checkDirectorAdded(Director $potentialDirector)
    {
        foreach ($this->getDirector() as $director) {
            if ($director->getId() == $potentialDirector->getId()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if Writer already exist on Relationship, avoid duplicate entries
     * @param Writer $potentialWriter
     * @return boolean
     */
    public function checkWriterAdded(Writer $potentialWriter)
    {
        foreach ($this->getWriter() as $writer) {
            if ($writer->getId() == $potentialWriter->getId()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if Production already exist on Relationship, avoid duplicate entries
     * @param Production $potentialProduction
     * @return boolean
     */
    public function checkProductionAdded(Production $potentialProduction)
    {
        foreach ($this->getProduction() as $production) {
            if ($production->getId() == $potentialProduction->getId()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Set composer
     *
     * @param array $composer
     *
     * @return Movie
     */
    public function setComposer($composer)
    {
        $this->composer = $composer;

        return $this;
    }

    /**
     * Get composer
     *
     * @return array
     */
    public function getComposer()
    {
        return $this->composer;
    }

    /**
     * Get composer
     *
     * @return array
     */
    public function addComposer(Composer $composer)
    {
        $this->composer[] = $composer;

        return $this;
    }

    /**
     * Check if Composer already exist on Relationship, avoid duplicate entries
     * @param Composer $potentialComposer
     * @return boolean
     */
    public function checkComposerAdded(Composer $potentialComposer)
    {
        foreach ($this->getComposer() as $composer) {
            if ($composer->getId() == $potentialComposer->getId()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return text
     */
    public function getAwards()
    {
        return $this->awards;
    }

    /**
     * @param text $awards
     */
    public function setAwards($awards)
    {
        $this->awards = $awards;
    }

    /**
     * @return float
     */
    public function getImdbScore()
    {
        return $this->imdbScore;
    }

    /**
     * @param float $imdbScore
     */
    public function setImdbScore($imdbScore)
    {
        $this->imdbScore = $imdbScore;
    }

    /**
     * @return int
     */
    public function getRottenTomatoesScore()
    {
        return $this->rottenTomatoesScore;
    }

    /**
     * @param int $rottenTomatoesScore
     */
    public function setRottenTomatoesScore($rottenTomatoesScore)
    {
        $this->rottenTomatoesScore = $rottenTomatoesScore;
    }

    /**
     * @return int
     */
    public function getMetacriticScore()
    {
        return $this->metacriticScore;
    }

    /**
     * @param int $metacriticScore
     */
    public function setMetacriticScore($metacriticScore)
    {
        $this->metacriticScore = $metacriticScore;
    }

    /**
     * @return int
     */
    public function getBudget()
    {
        return $this->budget;
    }

    /**
     * @param int $budget
     */
    public function setBudget($budget)
    {
        $this->budget = $budget;
    }

    /**
     * @return int
     */
    public function getRevenue()
    {
        return $this->revenue;
    }

    /**
     * @param int $revenue
     */
    public function setRevenue($revenue)
    {
        $this->revenue = $revenue;
    }

    /**
     * Set production
     *
     * @param Production $production
     *
     * @return Movie
     */
    public function setProduction(Production $production)
    {
        $this->production = $production;

        return $this;
    }

    /**
     * Get production
     *
     * @return Production
     */
    public function getProduction()
    {
        return $this->production;
    }

    /**
     * Add production
     *
     * @param Production $production
     *
     * @return Movie
     */
    public function addProduction(Production $production)
    {
        $this->production[] = $production;

        return $this;
    }

    /**
     * Set producer
     *
     * @param Producer $producer
     *
     * @return Movie
     */
    public function setProducer(Producer $producer)
    {
        $this->producer = $producer;

        return $this;
    }

    /**
     * Get producer
     *
     * @return producer
     */
    public function getProducer()
    {
        return $this->producer;
    }

    /**
     * Add producer
     *
     * @param Producer $producer
     *
     * @return Movie
     */
    public function addProducer(Producer $producer)
    {
        $this->producer[] = $producer;

        return $this;
    }

    /**
     * Set writer
     *
     * @param array $writer
     *
     * @return Movie
     */
    public function setWriter($writer)
    {
        $this->writer = $writer;

        return $this;
    }

    /**
     * Set writer
     *
     * @param Writer $writer
     *
     * @return Movie
     */
    public function addWriter(Writer $writer)
    {
        $this->writer[] = $writer;

        return $this;
    }

    /**
     * Get writer
     *
     * @return array
     */
    public function getWriter()
    {
        return $this->writer;
    }


    /**
     * Check if Producer already exist on Relationship, avoid duplicate entries
     * @param Production $potentialProducer
     * @return boolean
     */
    public function checkProducerAdded(Producer $potentialProducer)
    {
        foreach ($this->getProduction() as $production) {
            if ($production->getProducer() == $potentialProducer) {
                return true;
            }
        }

        return false;
    }

    /**
     * Set duration
     *
     * @param integer $duration
     *
     * @return Movie
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return int
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set genre
     *
     * @param array $genre
     *
     * @return Movie
     */
    public function setGenre($genre)
    {
        $this->genre = $genre;

        return $this;
    }

    /**
     * Get genre
     *
     * @return array
     */
    public function getGenre()
    {
        return $this->genre;
    }

    /**
     * Get genre
     *
     * @return Movie
     */
    public function addGenre(Genre $genre)
    {
        $this->genre[] = $genre;

        return $this;
    }


    /**
     * Set Keyword
     *
     * @param Keyword $keyword
     *
     * @return Movie
     */
    public function setKeyword($keyword)
    {
        $this->keyword = $keyword;

        return $this;
    }

    /**
     * Get Keyword
     *
     * @return array
     */
    public function getKeyword()
    {
        return $this->keyword;
    }

    /**
     * @param Keyword $keyword
     * @return $this
     */
    public function addKeyword(Keyword $keyword)
    {
        $this->keyword[] = $keyword;

        return $this;
    }


    /**
     * Set poster
     *
     * @param boolean $poster
     *
     * @return Movie
     */
    public function setPoster($poster)
    {
        $this->poster = $poster;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getScreenshot()
    {
        return $this->screenshot;
    }

    /**
     * @param boolean $screenshot
     */
    public function setScreenshot($screenshot)
    {
        $this->screenshot = $screenshot;
    }


    /**
     * Get poster
     *
     * @return array
     */
    public function getPoster()
    {
        return $this->poster;
    }

    /**
     * Set releaseInfo
     *
     * @param array $releaseInfo
     *
     * @return Movie
     */
    public function setReleaseInfo($releaseInfo)
    {
        $this->releaseInfo = $releaseInfo;

        return $this;
    }

    /**
     * Get releaseInfo
     *
     * @return array
     */
    public function getReleaseInfo()
    {
        return $this->releaseInfo;
    }

    /**
     * Set trailer
     *
     * @param array $trailer
     *
     * @return Movie
     */
    public function setTrailer($trailer)
    {
        $this->trailer = $trailer;

        return $this;
    }

    /**
     * Get trailer
     *
     * @return array
     */
    public function getTrailer()
    {
        return $this->trailer;
    }

    /**
     * Set score
     *
     * @param float $score
     *
     * @return Movie
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score
     *
     * @return float
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * @return float
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param integer $year
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getreleased()
    {
        return $this->released;
    }

    /**
     * @param \DateTime $released
     */
    public function setreleased($released)
    {
        $this->released = $released;
    }

    /**
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param Country $country
     */
    public function setCountry($country): Country
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @param Country $country
     */
    public function addCountry(Country $country)
    {
        $this->country[] = $country;

        return $this;
    }

    /**
     * @return string
     */
    public function getImdbId()
    {
        return $this->imdbId;
    }

    /**
     * @param string $imdbId
     */
    public function setImdbId($imdbId)
    {
        $this->imdbId = $imdbId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCharacter()
    {
        return $this->character;
    }

    /**
     * @param mixed $character
     */
    public function setCharacter($character)
    {
        $this->character = $character;
    }

    /**
     * @param Character $character
     * @return $this
     */
    public function addCharacter(Character $character)
    {
        $this->character[] = $character;

        return $this;
    }

    /**
     * @return text
     */
    public function getStoryline()
    {
        return $this->storyline;
    }

    /**
     * @param text $storyline
     */
    public function setStoryline($storyline)
    {
        $this->storyline = $storyline;
    }


    /**
     * @return string
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param string $rating
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
    }

    /**
     * @return boolean
     */
    public function isNeedReview()
    {
        return $this->needReview;
    }

    /**
     * @param boolean $needReview
     */
    public function setNeedReview($needReview)
    {
        $this->needReview = $needReview;
    }


    /**
     * Check if Cast already exist on Relationship, avoid duplicate entries
     * @param Cast $potentialCast
     * @return boolean
     */
    public function checkCastAdded(Cast $potentialCast)
    {
        if (!is_null($this->getCharacter())) {
            foreach ($this->getCharacter() as $character) {
                if ($character->getCast()->getId() == $potentialCast->getId()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param int $imdbRating
     * @param int $metacriticRating
     * @param int $rottenTomatoesRating
     * @param int $myRating
     * @param int $imdbVotes
     * @return $this
     */
    public function recalculateRating(
        $imdbRating = 5,
        $metacriticRating = 50,
        $rottenTomatoesRating = 50,
        $myRating = 5,
        $imdbVotes = 0
    ) {
        $imdbRating = (is_null($this->getImdbScore()) ? $imdbRating : $this->getImdbScore());
        $metacriticRating = (is_null($this->getMetacriticScore()) ? $metacriticRating : $this->getMetacriticScore());
        $rottenTomatoesRating = (is_null($this->getRottenTomatoesScore()) ? $rottenTomatoesRating : $this->getRottenTomatoesScore());
        // Sum of Scales must be $scale
        switch (true) {
            case ($imdbVotes < 5000):
                $imdbScale = 3;
                $metacriticSacale = 3;
                $rottenTomatoesScale = 3;
                break;
            case ($imdbVotes < 50000):
                $imdbScale = 4;
                $metacriticSacale = 2;
                $rottenTomatoesScale = 2;
                break;
            case ($imdbVotes < 500000):
                $imdbScale = 5;
                $metacriticSacale = 2;
                $rottenTomatoesScale = 2;
                break;
            case ($imdbVotes < 1000000):
                $imdbScale = 6;
                $metacriticSacale = 2;
                $rottenTomatoesScale = 1;
                break;
            default;
                $imdbScale = 3;
                $metacriticSacale = 3;
                $rottenTomatoesScale = 3;
        }
        $myScale = 1;
        $scale = 10;

        $this->score = round((($imdbRating * $imdbScale) / $scale), 2)
            + round((($rottenTomatoesRating * $rottenTomatoesScale) / ($scale * $scale)), 2)
            + round((($metacriticRating * $metacriticSacale) / ($scale * $scale)), 2)
            + round((($myRating * $myScale) / $scale), 2);

        return $this;
    }

    /**
     * @param bool $absolute
     * @return string
     */
    public function getScreenshootPath(bool $absolute)
    {
        if ($absolute === true) {
            return self::SCREENSHOT_PATH. $this->id. '.jpg';
        }
        return self::SCREENSHOT_PATH. $this->id. '.jpg';
    }

    public function setData(array $data)
    {
        if (isset($data['title']) && is_null($this->title)) {
            $this->title = $data['title'];
        }
        if (isset($data['year']) && is_null($this->year)) {
            $this->year = $data['year'];
        }
        if (isset($data['release']) && is_null($this->released)) {
            $date = new \DateTime();
            $this->released = $date->setTimestamp($data['release']);
        }
        if (isset($data['duration']) && is_null($this->duration)) {
            $this->duration = $data['duration'];
        }
        if (isset($data['plot']) && is_null($this->plot)) {
            $this->plot = $data['plot'];
        }
        if (isset($data['storyline']) && ($this->plot != $data['storyline'])) {
            $this->storyline = $data['storyline'];
        }
        if (isset($data['budget']) && is_null($this->budget)) {
            $this->budget = $data['budget'];
        }
        if (isset($data['revenue']) && is_null($this->revenue)) {
            $this->revenue = $data['revenue'];
        }
        if (isset($data['imdbRating']) && !is_null($data['imdbRating'])) {
            $this->imdbScore = $data['imdbRating'];
        }
        if (isset($data['rottenTomatoesRating']) && !is_null($data['rottenTomatoesRating'])) {
            $this->rottenTomatoesScore = $data['rottenTomatoesRating'];
        }
        if (isset($data['metacriticRating']) && !is_null($data['metacriticRating'])) {
            $this->metacriticScore = $data['metacriticRating'];
        }
        if (isset($data['awards']) && is_null($this->awards)) {
            $this->awards = $data['awards'];
        }
        if (isset($data['imdbID']) && is_null($this->imdbId)) {
            $this->imdbId = $data['imdbID'];
        }
        if (isset($data['rated']) && is_null($this->rating)) {
            $this->rating = $data['rated'];
        }

        $this->recalculateRating($this->imdbScore, $this->metacriticScore, $this->rottenTomatoesScore, null, null);
    }
}

