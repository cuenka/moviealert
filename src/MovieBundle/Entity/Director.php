<?php

namespace MovieBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Director
 *
 * @ORM\Table(name="directors", indexes={
 * @ORM\Index(name="director_id_idx", columns={"id"}),
 * @ORM\Index(name="director_slug_idx", columns={"slug"})
 * })
 * @ORM\Entity(repositoryClass="MovieBundle\Repository\DirectorRepository")
 */
class Director
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, unique=true)
     */
    private $slug;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="published", type="datetime")
     */
    private $published;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime")
     */
    private $modified;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthday", type="datetime", nullable=true)
     */
    private $birthday;

    /**
     * @var string
     *
     * @ORM\Column(name="thumbnail", type="boolean")
     */
    private $thumbnail = false;

    /**
     * @var string
     *
     * @ORM\Column(name="imdbId", type="string", length=50, unique=true, nullable=true)
     */
    private $imdbId;

    /**
     * @var string
     *
     * @ORM\Column(name="biography", type="text", nullable=true)
     */
    private $biography;

    /**
     * Many Directors have Many Films.
     * @ORM\ManyToMany(targetEntity="Movie", mappedBy="director")
     */
    private $filmography;

    /**
     * @var boolean
     *
     * @ORM\Column(name="needReview", type="boolean")
     */
    private $needReview = true;

    /**
     * Director constructor.
     */
    public function __construct()
    {
        $this->published = new \DateTime();
        $this->modified = new \DateTime();
        $this->filmography = new ArrayCollection();
        $this->thumbnail = false;
        $this->needReview = true;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Director
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Director
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set published
     *
     * @param \DateTime $published
     *
     * @return Director
     */
    public function setPublished($published)
    {
        $this->published = $published;

        return $this;
    }

    /**
     * Get published
     *
     * @return \DateTime
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     *
     * @return Director
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     *
     * @return Director
     */
    public function setAge($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime
     */
    public function getAge()
    {
        return $this->birthday;
    }

    /**
     * Set thumbnail
     *
     * @param string $thumbnail
     *
     * @return Director
     */
    public function setThumbnail($thumbnail)
    {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    /**
     * Get thumbnail
     *
     * @return string
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * Set filmography
     *
     * @param  ArrayCollection $filmography
     *
     * @return Director
     */
    public function setFilmography(ArrayCollection $filmography)
    {
        $this->filmography = $filmography;

        return $this;
    }

    /**
     * Get filmography
     *
     * @return ArrayCollection
     */
    public function getFilmography()
    {
        return $this->filmography;
    }

    /**
     * @param Movie $movie
     * @return $this
     */
    public function addFilm(Movie $movie)
    {
        $this->filmography[] = $movie;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * @param \DateTime $birthday
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    }

    /**
     * @return string
     */
    public function getBiography()
    {
        return $this->biography;
    }

    /**
     * @param $biography
     * @return $this
     */
    public function setBiography($biography)
    {
        $this->biography = $biography;

        return $this;
    }


    /**
     * @return string
     */
    public function getImdbId()
    {
        return $this->imdbId;
    }

    /**
     * @param $imdbId
     * @return $this
     */
    public function setImdbId($imdbId)
    {
        $this->imdbId = $imdbId;

        return $this;
    }


    /**
     * @return boolean
     */
    public function isNeedReview()
    {
        return $this->needReview;
    }

    /**
     * @param boolean $needReview
     */
    public function setNeedReview($needReview)
    {
        $this->needReview = $needReview;
    }
}
