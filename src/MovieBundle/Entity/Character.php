<?php

namespace MovieBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Character
 *
 * @ORM\Table(name="characters", indexes={
 * @ORM\Index(name="character_id_idx", columns={"id"})
 * })
 * @ORM\Entity(repositoryClass="MovieBundle\Repository\CharacterRepository")
 */
class Character
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="published", type="datetime")
     */
    private $published;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime")
     */
    private $modified;



    /**
     * Many Characters have One movie.
     * @ORM\ManyToOne(targetEntity="Movie", inversedBy="character", cascade={"persist"})
     * @ORM\JoinColumn(name="movie", referencedColumnName="id")
     */
    private $movie;

    /**
     * Many Characters are one actor or actress.
     * @ORM\ManyToOne(targetEntity="Cast", inversedBy="character")
     * @ORM\JoinColumn(name="cast", referencedColumnName="id")
     */
    private $cast;

    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", length=150, unique=false, nullable=true)
     */
    private $role;

    /**
     * Character constructor.
     * @param \DateTime $published
     * @param \DateTime $modified
     */
    public function __construct()
    {
        $this->published = new \DateTime();
        $this->modified = new \DateTime();
        $this->movie = new ArrayCollection();
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * @param \DateTime $published
     */
    public function setPublished($published)
    {
        $this->published = $published;
    }

    /**
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * @param \DateTime $modified
     */
    public function setModified($modified)
    {
        $this->modified = $modified;
    }

    /**
     * @return mixed
     */
    public function getMovie()
    {
        return $this->movie;
    }

    /**
     * @param mixed $movie
     */
    public function setMovie($movie)
    {
        $this->movie = $movie;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCast()
    {
        return $this->cast;
    }

    /**
     * @param mixed $cast
     */
    public function setCast($cast)
    {
        $this->cast = $cast;

        return $this;
    }

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param string $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }
}

