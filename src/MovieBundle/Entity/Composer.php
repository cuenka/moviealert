<?php

namespace MovieBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Composer
 *
 * @ORM\Table(name="composers", indexes={
 * @ORM\Index(name="id_idx", columns={"id"}),
 * @ORM\Index(name="slug_idx", columns={"slug"})
 * })
 * @ORM\Entity(repositoryClass="MovieBundle\Repository\ComposerRepository")
 */
class Composer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, unique=true)
     */
    private $slug;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="published", type="datetime")
     */
    private $published;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime")
     */
    private $modified;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthday", type="datetime")
     */
    private $birthday;

    /**
     * Many Composers compose many movies
     * @ORM\ManyToMany(targetEntity="Movie", mappedBy="composer")
     */
    private $filmography;


    /**
     * @var string
     *
     * @ORM\Column(name="imdbId", type="string", length=50, unique=true)
     */
    private $imdbId;


    /**
     * @var string
     *
     * @ORM\Column(name="biography", type="text")
     */
    private $biography;

    /**
     * @var boolean
     *
     * @ORM\Column(name="thumbnail", type="boolean")
     */
    private $thumbnail = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="needReview", type="boolean")
     */
    private $needReview = true;


    /**
     * Composer constructor.
     */
    public function __construct()
    {
        $this->filmography = new ArrayCollection();
        $this->published = new \DateTime();
        $this->modified = new \DateTime();
        $this->needReview = true;
        $this->thumbnail = false;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * @param \DateTime $published
     */
    public function setPublished($published)
    {
        $this->published = $published;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * @param \DateTime $modified
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * @param \DateTime $birthday
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFilmography()
    {
        return $this->filmography;
    }

    /**
     * @param mixed $filmography
     */
    public function setFilmography($filmography)
    {
        $this->filmography = $filmography;
    }

    /**
     * @param mixed $filmography
     */
    public function addFilm($filmography)
    {
        $this->filmography[] = $filmography;

        return $this;
    }

    /**
     * @return string
     */
    public function getImdbId()
    {
        return $this->imdbId;
    }

    /**
     * @param string $imdbId
     */
    public function setImdbId($imdbId)
    {
        $this->imdbId = $imdbId;
    }

    /**
     * @return string
     */
    public function getBiography()
    {
        return $this->biography;
    }

    /**
     * @param $biography
     * @return $this
     */
    public function setBiography($biography)
    {
        $this->biography = $biography;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * @param boolean $thumbnail
     */
    public function setThumbnail($thumbnail)
    {
        $this->thumbnail = $thumbnail;
    }

    /**
     * @return boolean
     */
    public function isNeedReview()
    {
        return $this->needReview;
    }

    /**
     * @param boolean $needReview
     */
    public function setNeedReview($needReview)
    {
        $this->needReview = $needReview;
    }
}

