<?php

namespace MovieBundle\Controller;

use MovieBundle\Entity\Keyword;
use MovieBundle\Entity\Movie;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class KeywordController
 * @package MovieBundle\Controller
 * @Route("/keyword", name="keywords")
 */
class KeywordController extends Controller
{

    /**
     * @Route("/top/{slug}", name="keyword_top")
     */
    public function keywordTopAction(Request $request, $slug)
    {
        $keyword = $this->getDoctrine()->getRepository('MovieBundle:Keyword')->findOneBy(['slug' => $slug]);
        $movies = $this->getDoctrine()->getRepository('MovieBundle:Movie')->findBykeyword($keyword);
        $screenshot = isset($movies[0]) ? $movies[0]->getId() : 0;

        return $this->render('MovieBundle:Keyword:list.html.twig', array(
            'screenshot' => $screenshot,
            'movies'    => $movies,
            'keyword'    => $keyword,
            'imagePath' => '/images/movie/'
        ));
    }
}
