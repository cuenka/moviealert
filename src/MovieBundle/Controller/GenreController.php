<?php

namespace MovieBundle\Controller;

use MovieBundle\Entity\Genre;
use MovieBundle\Entity\Movie;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class GenreController
 * @package MovieBundle\Controller
 * @Route("/genre", name="genres")
 */
class GenreController extends Controller
{

    /**
     * @Route("/top/{slug}", name="genre_top")
     */
    public function genreTopAction(Request $request, $slug)
    {
        $genre = $this->getDoctrine()->getRepository('MovieBundle:Genre')->findOneBy(['slug' => $slug]);
        $movies = $this->getDoctrine()->getRepository('MovieBundle:Movie')->findByTopMovies($genre);

        $screenshot = isset($movies[0]) ? $movies[0]->getId() : 0;

        return $this->render('MovieBundle:Genre:list.html.twig', array(
            'screenshot' => $screenshot,
            'movies'    => $movies,
            'genre'    => $genre,
            'imagePath' => '/images/movie/'
        ));
    }
}
