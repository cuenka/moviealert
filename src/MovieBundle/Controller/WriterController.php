<?php

namespace MovieBundle\Controller;

use MovieBundle\Entity\Movie;
use MovieBundle\Repository\WriterRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class WriterController
 * @package MovieBundle\Controller
 * @Route("/writer", name="writers")
 */
class WriterController extends Controller
{

    /**
     * @Route("/{slug}", name="writer")
     */
    public function writerAction(Request $request, $slug)
    {
        $writer = $this->getDoctrine()->getRepository('MovieBundle:Writer')->findBySlug($slug);
        $filmography = $writer->getFilmography();

        return $this->render('MovieBundle:Writer:Writer.html.twig',
            [
                'writer' =>$writer,
                'filmography' =>$filmography,
                'imagePath' => '/images/movie/'
            ]);
    }

}
