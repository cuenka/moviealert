<?php

namespace MovieBundle\Controller;

use MovieBundle\Entity\Movie;
use MovieBundle\Repository\CastRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CastController
 * @package MovieBundle\Controller
 * @Route("/cast", name="casting")
 */
class CastController extends Controller
{

    /**
     * @Route("/{slug}", name="cast")
     */
    public function castAction(Request $request, $slug)
    {
        $cast = $this->getDoctrine()->getRepository('MovieBundle:Cast')->findBySlug($slug);
        $filmography = $this->getDoctrine()->getRepository('MovieBundle:Cast')->getMoviesByCast($cast->getId());
        $screenshot = isset($filmography[0]) ? $filmography[0]->getId() : 0;

        return $this->render('MovieBundle:Cast:Cast.html.twig',
            [
                'screenshot' => $screenshot,
                'cast' =>$cast,
                'filmography' =>$filmography,
                'imagePath' => '/images/movie/'
            ]);
    }

}
