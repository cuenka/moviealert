<?php

namespace MovieBundle\Controller;

use MovieBundle\Entity\Movie;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class MovieBundle
 * @package MovieBundle\Controller
 * @Route("/composer", name="composer")
 */
class ComposerController extends Controller
{

    /**
     * @Route("/{slug}", name="composer_view")
     */
    public function composerAction(Request $request, $slug)
    {
        $movie = $this->getDoctrine()->getRepository('MovieBundle:Composer')->findBySlug($slug);

        return $this->render('MovieBundle:Composer:view.html.twig',
            [
                'movie' =>$movie
            ]);
    }
}
