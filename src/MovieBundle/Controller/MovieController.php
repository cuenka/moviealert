<?php

namespace MovieBundle\Controller;

use MovieBundle\Entity\Movie;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class MovieBundle
 * @package MovieBundle\Controller
 * @Route("/movie", name="movies")
 */
class MovieController extends Controller
{

    /**
     * @Route("/{slug}", name="movie")
     */
    public function movieAction(Request $request, $slug)
    {
        $twig = $this->get('twig');
        $twig->addExtension(new \Twig_Extensions_Extension_Intl());
        $movie = $this->getDoctrine()->getRepository('MovieBundle:Movie')->findBySlug($slug);
        return $this->render('MovieBundle:Movie:movie.html.twig', [
                'movie' =>$movie
            ]);
    }
}
