<?php

namespace MovieBundle\Controller;

use MovieBundle\Entity\Director;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DirectorController
 * @package MovieBundle\Controller
 * @Route("/director", name="directors")
 */
class DirectorController extends Controller
{

    /**
     * @Route("/{slug}", name="director")
     */
    public function movieAction(Request $request, $slug)
    {
        $director = $this->getDoctrine()->getRepository('DirectorBundle:Movie')->findBySlug($slug);

        return $this->render('MovieBundle:Director:Director.html.twig',
            [
                'director' =>$director
            ]);
    }
}
