<?php

namespace MovieBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class MovieType
 * @package MovieBundle\Form
 */
class MovieType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('published', DateTimeType::class,
                [
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ])
            ->add('updated', DateTimeType::class,
                [
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ])
            ->add('title', TextType::class,
                [
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ])
            ->add('slug', TextType::class,
                [
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ])
            ->add('language', TextType::class,
                [
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ])
            ->add('castList', TextType::class,
                [
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ])
            ->add('director', TextType::class,
                [
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ])
            ->add('composer', TextType::class,
                [
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ])
            ->add('producer', TextType::class,
                [
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ])
            ->add('writer', TextType::class,
                [
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ])
            ->add('duration', TextType::class,
                [
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ])
            ->add('genre', TextType::class,
                [
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ])
            ->add('poster', TextType::class,
                [
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ])
            ->add('releaseInfo', TextType::class,
                [
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ])
            ->add('duration', TextType::class,
                [
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ])
            ->add('trailer', TextType::class,
                [
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ])
            ->add('score', TextType::class,
                [
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ])
            ->add('year', TextType::class,
                [
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ])
            ->add('country', TextType::class,
                [
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ])
            ->add('imdbId', TextType::class,
                [
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ])
                ->add('save', SubmitType::class,
        array(
            'label' => 'Save',
            'attr' => array (
                'class' => 'btn btn-success'
            )
        )
    );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'MovieBundle\Entity\Movie',
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'moviebundle_movie';
    }


}
