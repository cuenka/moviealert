<?php

namespace MovieBundle\Helper;

/**
 * Class CountryHelper
 * @package MovieBundle\Helper
 */
class LanguageHelper
{
    /**
     * @param string $country
     * @return null|string
     */
    public function resolveSynonyms($languages)
    {
        $options = [$languages, utf8_encode($languages)];
        $english = ['En', ''];
        $mandarin = ['普通话', 'æ®éè¯'];
        $french = ['Français', 'FR', 'Fr'];
        $hungarian = ['Hu', 'hungarian', 'Magyar'];
        $italian = ['Italiano', 'italiano', 'It'];
        $spanish = ['Español', 'español', 'Es', 'Català', 'Catala', 'Catalan'];
        $japanese = ['japanese', '日本語', 'Ja', 'ja', 'æ¥æ¬èª'];
        $german = ['Ge', 'ge', 'german', 'Deutsch', 'De'];
        $russian = ['Pусский', 'Ru'];
        $icelandic = ['Íslenska', 'Is'];
        $turkish = ['Türkçe', 'TU', 'Tu', 'turkish'];
        $danish = ['Dansk', 'Da', 'da', 'danish'];
        $portuguese = ['portuguese', 'Pt', 'Português'];
        $greek = ['ελληνικά', 'ÎµÎ»Î»Î·Î½Î¹ÎºÎ¬'];
        $norwegian = ['norwegian', 'norsk', 'Norsk', 'Nynorsk', 'Bokmål'];
        $romanian = ['Română'];
        $czech = ['Český'];
        $arabic = ['العربية'];
        $irish = ['gaeilge', 'Gaeilge'];
        $dutch = ['Nederlands', 'Frisian', 'Papiamento'];
        $yueChinese = ['广州话 / 廣州話'];
        $serbian = ['Srpski'];
        $hebrew = ['עִבְרִית'];
        $punjabi = ['ਪੰਜਾਬੀ'];
        $thai = ['ภาษาไทย'];
        $pashto = ['پښتو'];
        $urdu = ['اردو'];
        $ukrainian = ['Український'];
        $albanian = ['Shqip'];
        $hindi = ['हिन्दी'];
        $swedish = ['Svenska', 'Sv'];
        $vietnamese = ['Tiếng Việt'];
        $korean = ['한국어/조선말'];
        $kazakh = ['қазақ'];
        $slovak = ['Slovenčina', 'slovenský'];
        $slovenian = ['Slovenščina'];
        $persian = ['فارسی', 'Farsi'];
        $finnish = ['Fi', 'fi-FI'];

        foreach ($options as $language) {
            if (in_array($language, $english)) {
                return 'English';
            }
            if (in_array($language, $mandarin)) {
                return 'Mandarin';
            }
            if (in_array($language, $french)) {
                return 'French';
            }
            if (in_array($language, $hungarian)) {
                return 'Hungarian';
            }
            if (in_array($language, $italian)) {
                return 'Italian';
            }
            if (in_array($language, $spanish)) {
                return 'Spanish';
            }
            if (in_array($language, $japanese)) {
                return 'Japanese';
            }
            if (in_array($language, $german)) {
                return 'German';
            }
            if (in_array($language, $russian)) {
                return 'Russian';
            }
            if (in_array($language, $icelandic)) {
                return 'Icelandic';
            }
            if (in_array($language, $turkish)) {
                return 'Turkish';
            }
            if (in_array($language, $danish)) {
                return 'Danish';
            }
            if (in_array($language, $greek)) {
                return 'Greek';
            }
            if (in_array($language, $portuguese)) {
                return 'Portuguese';
            }
            if (in_array($language, $norwegian)) {
                return 'Norwegian';
            }
            if (in_array($language, $romanian)) {
                return 'Romanian';
            }
            if (in_array($language, $czech)) {
                return 'Czech';
            }
            if (in_array($language, $arabic)) {
                return 'Arabic';
            }
            if (in_array($language, $irish)) {
                return 'Irish';
            }
            if (in_array($language, $dutch)) {
                return 'Dutch';
            }
            if (in_array($language, $yueChinese)) {
                return 'Yue Chinese';
            }
            if (in_array($language, $serbian)) {
                return 'Serbian';
            }
            if (in_array($language, $hebrew)) {
                return 'Hebrew';
            }
            if (in_array($language, $punjabi)) {
                return 'Punjabi';
            }
            if (in_array($language, $thai)) {
                return 'Thai';
            }
            if (in_array($language, $pashto)) {
                return 'Pashto';
            }
            if (in_array($language, $urdu)) {
                return 'Urdu';
            }
            if (in_array($language, $ukrainian)) {
                return 'Ukrainian';
            }
            if (in_array($language, $albanian)) {
                return 'Albanian';
            }
            if (in_array($language, $hindi)) {
                return 'Hindi';
            }
            if (in_array($language, $swedish)) {
                return 'Swedish';
            }
            if (in_array($language, $vietnamese)) {
                return 'Vietnamese';
            }
            if (in_array($language, $korean)) {
                return 'Korean';
            }
            if (in_array($language, $kazakh)) {
                return 'Kazakh';
            }
            if (in_array($language, $slovak)) {
                return 'Slovak';
            }
            if (in_array($language, $slovenian)) {
                return 'Slovak';
            }
            if (in_array($language, $persian)) {
                return 'Persian';
            }
            if (in_array($language, $finnish)) {
                return 'Finnish';
            }

        }

        return $languages;
    }
}