<?php

namespace MovieBundle\Helper;

/**
 * Class GenreHelper
 * @package MovieBundle\Helper
 */
class GenreHelper
{
    /**
     * @param string $genre
     * @return null|string
     */
    public function resolveSynonyms($genre) {
        $sciFi = ['Sci-Fi', 'Fiction'];

        if (in_array($genre, $sciFi)) {
            return 'Science Fiction';
        }
        return $genre;
    }
}