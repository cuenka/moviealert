<?php

namespace MovieBundle\Repository;

use Doctrine\ORM\EntityRepository;
use MovieBundle\Entity\Director;

/**
 * CastRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class DirectorRepository extends EntityRepository
{
    const LIMIT = 2000;
    const SORT_LIMIT = 5;

    const NAME = 'MovieBundle:Director';
    /**
     * @param string $imdbId
     * @return array|\MovieBundle\Entity\Director[]
     */
    public function findByIMDBId($imdbId)
    {
        return $this->createQueryBuilder('d')
            ->select('count(d.id)')
            ->where('d.imdbId = :imdbId')
            ->setParameter('imdbId', $imdbId)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param string $name
     * @return array|\MovieBundle\Entity\Director[]
     */
    public function findByName($name)
    {
        return $this->createQueryBuilder('d')
            ->select('count(d.id)')
            ->where('d.name = :name')
            ->setParameter('name', $name)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Get info for table
     * @return array
     */
    public function getInfoForTable($repository, $select, $offset)
    {
        return $this->getEntityManager()
            ->getRepository($repository)
            ->createQueryBuilder('c')
            ->select($select)
            ->setFirstResult($offset)
            ->setMaxResults(self::LIMIT)
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);
    }

    /**
     * @param $slug
     * @return null|object
     */
    public function findBySlug($slug)
    {
        return $this
            ->findOneBy(
                [
                    'slug'=> $slug
                ]
            );
    }

    /**
     * @param string $name
     * @return boolean
     */
    public function findOneByNameIfExist(string $name): bool
    {
        $entity = $this
            ->findOneBy(
                [
                    'name'=> $name
                ]
            );

        if ($entity instanceof Director) {
            return true;
        }

        return false;
    }

    /**
     * @return array|\MovieBundle\Entity\Cast[]
     */
    public function findForReview()
    {
        return $this
            ->findBy(
                [
                    'needReview'=> true
                ],
                [
                    'id' => 'ASC'
                ],
                self::SORT_LIMIT
            );
    }
}
