<?php

namespace ImporterBundle\Api;

use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class Api
 */
class Api
{
    const URL= '';
    const KEY= '';

    protected $url;

    protected $key;

    protected $guzzle;

    /**
     * Api constructor.
     */
    public function __construct()
    {
    }
    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @return string
     */
    public function getBaseURL(): string
    {
        $baseUrl = $this->getUrl() . $this->getKey();
        return $baseUrl;
    }

    /**
     * @param array $params
     * @return string
     */
    protected function buildEndpoint(array $params, string $uri = null): string
    {
        return $uri . $this->getKey() . '&' . http_build_query($params);
    }

    /**
     * @param string $endPoint
     * @return array
     */
    protected function getData(string $endPoint): array
    {
        $response = $this->guzzle->get($endPoint);

        if ($response->getStatusCode() != 200) {
            return $data = [
                'Response'  => false,
                'Error'     => 'Not a 200 status code'
            ];
        }
        $data = json_decode($response->getBody(), true);

        return $data;
    }

    /**
     * @param $text
     * @return string
     */
    protected function cleanUpText($text)
    {
        return strip_tags($text);

    }

    /**
     * @param $text
     * @return string
     */
    protected function cleanUpName($name)
    {
        return ucwords(trim($this->cleanUpText($name)));

    }

    /**
     * @param array $response
     * @return array
     */
    protected function beautifyResponse(array $response): array
    {
        // Titles
        $response['title'] = null;
        $data['title'] = (isset($response['original_title']) &&
            $response['original_title'] != 'N/A' ) ? $response['original_title'] : null;
        if (isset($response['Title']) && ($data['title'] == null )) {
            $data['title'] = $response['Title'];
        }

        // Dates
        $data['year'] = null;
        $data['release'] = null;
        $data['year'] = (isset($response['Year']) &&
            $response['Year'] != 'N/A' ) ? intval($response['Year']) : null;

        if (isset($response['Released']) && ($response['Released']!= 'N/A')) {
            $data['release'] = strtotime($response['Released']);
        }
        if (isset($response['release_date']) && ($response['release_date']!= 'N/A')) {
            $data['release'] = strtotime($response['release_date']);
            $data['year'] = intval(substr($response['release_date'], 0, 4));
        }

        // Runtime
        $data['duration'] = null;
        if (isset($response['Runtime']) && ($response['Runtime']!= 'N/A')) {
            $data['duration'] = intval(str_replace('min', '', trim($response['Runtime'])));
        }
        if (isset($response['runtime']) && ($data['duration'] == null)) {
            $data['duration'] = intval(str_replace('min', '', trim($response['runtime'])));
        }

        // Genres
        $data['genre'] = null;
        if (isset($response['Genre']) && ($response['Genre']!= 'N/A')) {
            $data['genre'] = explode(',', str_replace(' ', '', $response['Genre']));
        }
        if (isset($response['genres'][0]) && ($data['genre'] == null)) {
            foreach($response['genres'] as $genre) {
                $data['genre'][] = $genre['name'];
            }
        }

        // Language
        $data['language'] = null;
        if (isset($response['spoken_languages']) && ($response['spoken_languages']!= 'N/A')) {
            foreach($response['spoken_languages'] as $language) {
                $data['language'][] = $language['name'];
            }
        }
        if (isset($response['original_language']) && ($data['language'] == null)) {
            $data['language'][] = $response['original_language'];
        }

        // Countries
        $data['country'] = null;
        if (isset($response['Country']) && ($response['Country']!= 'N/A')) {
            $data['country'] = explode(',', preg_replace('#\s*\(.+\)\s*#U', '', $response['Country']));
            for ($i = 0; $i < count($data['country']); $i++) {
                $data['country'][$i] = trim($data['country'][$i]);
            }
        }
        if (isset($response['production_countries'][0]) && ( $data['country'] == null)) {
            foreach($response['production_countries'] as $country) {
                $data['country'][] = $country['name'];
            }
        }

        // Director
        $data['director'] = null;
        if (isset($response['Director']) && ($response['Director']!= 'N/A')) {
            $data['director'] = explode(',', preg_replace('#\s*\(.+\)\s*#U', '', $response['Director']));
            for ($i = 0; $i < count($data['director']); $i++) {
                $data['director'][$i] = trim($data['director'][$i]);
            }
        }

        // Writer
        $data['writer'] = null;
        if (isset($response['Writer']) && ($response['Writer']!= 'N/A')) {
            $data['writer'] = explode(',', preg_replace('#\s*\(.+\)\s*#U', '', $response['Writer']));
            for ($i = 0; $i < count($data['writer']); $i++) {
                $data['writer'][$i] = trim($data['writer'][$i]);
            }
        }

        // Cast
        $data['cast'] = null;
        if (isset($response['Actors']) && ($response['Actors']!= 'N/A')) {
            $data['cast'] = explode(',', preg_replace('#\s*\(.+\)\s*#U', '', $response['Actors']));
            for ($i = 0; $i < count($data['cast']); $i++) {
                $data['cast'][$i] = trim($data['cast'][$i]);
            }
        }

        // Production
        $data['production'] = null;
        if (isset($response['Production']) && ($response['Production']!= 'N/A')) {
            $data['production'] = explode(',', preg_replace('#\s*\(.+\)\s*#U', '', $response['Production']));
            for ($i = 0; $i < count($data['production']); $i++) {
                $data['production'][$i] = trim($data['production'][$i]);
            }
        }


        if (isset($response['production_companies'][0]) && ($data['production'] == null)) {
            foreach($response['production_companies'] as $company) {
                $data['production'][] = $company['name'];
            }
        }

        // Keywords
        $data['keywords'] = null;
        if (isset($response['keywords']['keywords'])) {
            foreach($response['keywords']['keywords'] as $keyword) {
                $data['keywords'][] = $keyword['name'];
            }
        }


        // Plot and storyline
        $data['plot'] = null;
        $data['storyline'] = null;
        $data['plot'] = (isset($response['Plot']) &&
            $response['Plot'] != 'N/A' ) ? $response['Plot'] : null;
        $data['storyline'] = (isset($response['overview']) &&
            $response['overview'] != 'N/A' ) ? $response['overview'] : null;

        // images
        $data['poster'] = null;
        $data['backgroundPoster'] = null;
        $data['poster'] = (isset($response['Poster']) &&
            $response['Poster'] != 'N/A' ) ? $response['Poster'] : null;
        $data['poster'] = (isset($response['poster_path']) && ($data['poster'] == null )) ?
            'https://image.tmdb.org/t/p/w342' . $response['poster_path'] : null;
        $data['backgroundPoster'] = (isset($response['backdrop_path']) &&
            $response['backdrop_path'] != 'N/A' ) ?
            'https://image.tmdb.org/t/p/w780' . $response['backdrop_path'] : null;

        // Money
        $data['budget'] = null;
        $data['revenue'] = null;
        $data['budget'] = (isset($response['budget']) &&
            $response['budget'] != 'N/A' ) ? $response['budget'] : null;
        $data['revenue'] = (isset($response['BoxOffice']) &&
            $response['BoxOffice'] != 'N/A' ) ? $response['BoxOffice'] : null;
        if (isset($response['revenue']) && $data['revenue'] == null ) {
            $data['revenue'] = $response['revenue'];
        }
        //Rated
        $data['rated'] = (isset($response['Rated']) &&
            $response['Rated'] != 'N/A' ) ? $response['Rated'] : null;


        // Ratings
        $data['imdbRating'] = null;
        $data['rottenTomatoesRating'] = null;
        $data['metacriticRating'] = null;

        if (isset($response['Ratings']) && ($response['Ratings']!= 'N/A')) {
            foreach ($response['Ratings'] as $rating) {
                if ($rating['Source'] == 'Internet Movie Database') {
                    $data['imdbRating'] = floatval(str_replace('/10', '', $rating['Value']));
                }
                if ($rating['Source'] == 'Rotten Tomatoes') {
                    $data['rottenTomatoesRating'] = floatval(str_replace('%', '', $rating['Value']));
                }
                if ($rating['Source'] == 'Metacritic') {
                    $data['metacriticRating'] = floatval(str_replace('/100', '', $rating['Value']));
                }
            }
            if (is_null($data['metacriticRating']) &&
                isset($response['Metascore']) &&
                ($response['Metascore']!= 'N/A')) {
                $data['metacriticRating'] = $response['Metascore'];
            }
        }

        $response['imdbVotes'] = null;
        if (isset($response['imdbVotes']) && ($response['imdbVotes']!= 'N/A')) {
            $data['imdbVotes'] =intval(str_replace(',', '', trim($response['imdbVotes'])));
        }

        // Awards
        $data['awards'] = (isset($response['Awards']) &&
            $response['Awards'] != 'N/A' ) ? $response['Awards'] : null;

        // ImdbID
        $data['imdbID'] = (isset($response['imdb_id']) &&
            $response['imdb_id'] != 'N/A' ) ? $response['imdb_id'] : null;

        $data['type'] = (isset($response['Type']) &&
            $response['Type'] != 'N/A' ) ? $response['Type'] : 'movie';
        $data['adult'] = (isset($response['adult']) &&
            $response['adult'] != 'N/A' ) ? boolval($response['adult']) : null;

        return $data;
    }

    public function mergeResponses(array $data1, array $data2)
    {
        foreach( $data1 as $key => $value) {
            if ($data1[$key] === null) {

                $data1[$key] = isset($data2[$key])? $data2[$key] : null;
            }
        }

        return $data1;
    }

}