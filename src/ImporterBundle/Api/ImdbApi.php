<?php

namespace ImporterBundle\Api;

use GuzzleHttp\Client;
use Imdb\Config;
use Imdb\Person;
use Imdb\PersonSearch;

/**
 * Class OmdbApi
 */
class ImdbApi extends Api
{
    /**
     * @var Config $config
     */
    protected $config;

    /**
     * @var PersonSearch $person
     */
    protected $person;
    /**
     * Api constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->config = new Config();
        $this->config->language = 'en-US';
        $this->person = new PersonSearch($this->config);
    }


    public function searchOnePersonByName(string $name = 'Forest Whitaker', string $imdbId = null, array $role = []): array
    {
        $responses = $this->person->search($name);
        foreach($responses as $response) {
            $searchDetails = $response->getSearchDetails();
            if ((isset($searchDetails['role']) && (in_array($searchDetails['role'], $role)))) {
                if (!is_null($imdbId)) {
                    if ($response->imdbid() == $imdbId) {
                        return $this->beautifyPersonResponse($response, $searchDetails['role']);

                    }
                } else {
                    if ($response->name() == $name) {
                        return $this->beautifyPersonResponse($response, $searchDetails['role']);
                    }
                }
            }

        }

        return $this->beautifyPersonResponse($response, null);
    }

    /**
     * @param string $i
     * @return array
     */
    public function searchPersonByName(string $name = 'Forest Whitaker', string $imdbId = null): array
    {
        $results = $this->person->search($name);
//        var_dump($results);
        foreach($results as $result) {
        var_dump($result->bio());
            echo "<br>";
//            echo $result->born();
            echo "<br>";
//            echo $result->bio();
            echo "<br>";
            echo "<br>";
            echo "<br>";
        }
        die();
        return $this->beautifyResponse($response);
    }

    /**
     * @param string $t
     * @return array
     */
    public function searchByTitle(string $t = 'the Matrix'): array
    {
        $params['query'] = $t;
        $uri = 'search/movie';
        $endPoint = $this->buildEndpoint($params, $uri);
        $response = $this->getResponse($endPoint, self::TITLE);
        unset($params);
        $params = [];
        $endPoint = $this->buildEndpoint($params, 'movie/'. $response['results'][0]['id']);
        $response = $this->getResponse($endPoint, self::IMDBID);

        return $this->beautifyResponse($response);
    }

    /**
     * @param string $t
     * @return array
     */
    public function searchBySearching(string $s = 'the Matrix', $page = 1): array
    {
        $results = [];
        $pages = 1;
        $params['query'] = $s;
        $params['page'] = $page;
        $endPoint = $this->buildEndpoint($params, 'search/movie');
        $response = $this->getResponse($endPoint, self::SEARCHING);

        foreach ($response['results'] as $search) {
            $results[] = $this->beautifyResponse($search);
        }

        if (isset($response['total_results'])) {
            $pages = $response['total_results'] / 20;
        }

        if ($pages > 1) {
            // Pages
            for ($i = 2; $i <= $pages; $i++) {
                // Results per Page
                foreach ($response['Search'] as $search) {
                    $results[] = $this->beautifyResponse($search);
                }
                $params['page'] = $i;
                $endPoint = $this->buildEndpoint($params);
                $response = $this->getResponse($endPoint);
            }
        }
        return $results;
    }

    /**
     * @param \stdClass $response
     * @return array
     */
    protected function beautifyPersonResponse(Person $response, string $role = null): array
    {
        $data =
            [
                'name'        => $response->name(),
                'imdbId'      => $response->imdbid(),
                'role'        => null,
                'thumbnail'   => null,
                'biography'   => null,
                'birthday'   => null,
                'filmography' => []
            ];

        if(!empty($response->born())) {
            $data['birthday'] = $response->born();
        }
        if(!empty($response->movies_all())) {
            foreach($response->movies_all() as $movie) {
                if ($movie['title_type'] == 'Movie') {
                    $data['filmography'][] = $movie;
                }
            }
        }

        if (!empty($response->photo(true)) && substr($response->photo(true), -3) != 'png') {
            $data['thumbnail'] = $response->photo(true);
        }

        if (isset($response->bio()[0]['desc'])) {
            $data['biography'] = $this->cleanUpText($response->bio()[0]['desc']);
        }

        return $data;
    }
}