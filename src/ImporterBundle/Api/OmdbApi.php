<?php

namespace ImporterBundle\Api;

use GuzzleHttp\Client;

/**
 * Class OmdbApi
 */
class OmdbApi extends Api
{
    const URL= 'http://www.omdbapi.com/';
    const KEY= '?apikey=ad6d5fb5';

    /**
     * Api constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->url = self::URL;
        $this->key = self::KEY;
        $this->guzzle = new Client(['base_uri' => self::URL]);
    }

    /**
     * @param string $type
     * @param string $plot
     * @param string $r
     * @return array
     */
    private function setRequiredParameters(string $type = 'movie', string $plot = 'full', string $r = 'json'): array
    {
        return $params =
            [
                'type' => $type,
                'plot' => $plot,
                'r'    => $r
            ];
    }

    private function getResponse($endPoint)
    {
        $data = $this->getData($endPoint);
        if ($data['Response'] == 'False') {
            return $data = [
                'Response'  => false,
                'error'     => isset($data['Error']) ? $data['Error'] : 'Unkown'
            ];
        }

        return $data;
    }



    /**
     * @param string $i
     * @return array
     */
    public function searchByImdbID(string $i = 'tt1285016'): array
    {
        $params = $this->setRequiredParameters();
        $params['i'] = $i;
        $endPoint = $this->buildEndpoint($params);
        $response = $this->getResponse($endPoint);
        if (!$response['Response']) {
            return $response;
        }

        return $this->beautifyResponse($response);
    }

    /**
     * @param string $t
     * @return array
     */
    public function searchByTitle(string $t = 'the Matrix'): array
    {
        $params = $this->setRequiredParameters();
        $params['t'] = $t;
        $endPoint = $this->buildEndpoint($params);
        $response = $this->getResponse($endPoint);
        if (!$response['Response']) {
            return $response;
        }

        return $this->beautifyResponse($response);
    }

    /**
     * @param string $t
     * @return array
     */
    public function searchBySearching(string $s = 'the Matrix', $page = 1): array
    {
        $results = [];
        $pages = 1;
        $params = $this->setRequiredParameters();
        $params['s'] = $s;
        $params['page'] = $page;
        $endPoint = $this->buildEndpoint($params);
        $response = $this->getResponse($endPoint);
        if (!$response['Response']) {
            return $response;
        }
        if (isset($response['totalResults'])) {
            $pages = $response['totalResults'] / 10;
        }

        if ($pages > 1) {
            // Pages
            for ($i = 2; $i <= $pages; $i++) {
                // Results per Page
                foreach ($response['Search'] as $search) {
                    $results[] = $this->beautifyResponse($search);
                }
                $params['page'] = $i;
                $endPoint = $this->buildEndpoint($params);
                $response = $this->getResponse($endPoint);
            }
        }
        return $results;
    }

}