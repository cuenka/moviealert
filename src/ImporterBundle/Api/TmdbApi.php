<?php

namespace ImporterBundle\Api;

use GuzzleHttp\Client;

/**
 * Class OmdbApi
 */
class TmdbApi extends Api
{
    const URL= 'https://api.themoviedb.org/3/';
    const KEY= '?api_key=bd671e7433f2475fcfdd639ebc58156b';

    const IMDBID = 1;
    const TITLE = 2;
    const SEARCHING = 3;
    /**
     * Api constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->url = self::URL;
        $this->key = self::KEY;
        $this->guzzle = new Client(['base_uri' => self::URL]);
    }

    /**
     * @param $endPoint
     * @return array
     */
    private function getResponse($endPoint, $case): array
    {
        $data = $this->getData($endPoint);
        switch ($case) {
            case 1;
                if ((count($data) != 25) && (count($data) != 5) && (count($data) != 2)) {
                    return $data = [
                        'Response'  => false,
                        'error'     => 'No results'
                    ];
                }
                break;
            case 2;
                if ((!isset($data['total_results'])) || (($data['total_results'] == 0))) {
                    return $data = [
                        'Response'  => false,
                        'error'     => 'No results'
                    ];
                }
                break;
            case 3;
                if ((!isset($data['total_results'])) || (($data['total_results'] == 0))) {
                    return $data = [
                        'Response'  => false,
                        'error'     => 'No results'
                    ];
                }
                break;
        }


        return $data;
    }



    /**
     * @param string $i
     * @return array
     */
    public function searchByImdbID(string $i = 'tt1285016'): array
    {
        $params['external_source'] = 'imdb_id';
        $endPoint = $this->buildEndpoint($params, 'find/'. $i);
        $response = $this->getResponse($endPoint, self::IMDBID);
        if (!isset($response['movie_results'][0])) {
            return $data = [
                'Response'  => false,
                'error'     => 'No results'
            ];
        }
        unset($params);
        $params = [];
        $tmdbId = $response['movie_results'][0]['id'];
        $endPoint = $this->buildEndpoint($params, 'movie/'. $tmdbId);
        $response = $this->getResponse($endPoint, self::IMDBID);

        $endPoint = $this->buildEndpoint($params, 'movie/'. $tmdbId. '/keywords');
        $response['keywords'] = $this->getResponse($endPoint, self::IMDBID);

        return $this->beautifyResponse($response);
    }

    /**
     * @param string $t
     * @return array
     */
    public function searchByTitle(string $t = 'the Matrix'): array
    {
        $params['query'] = $t;
        $uri = 'search/movie';
        $endPoint = $this->buildEndpoint($params, $uri);
        $response = $this->getResponse($endPoint, self::TITLE);
        if (isset($response['error'])) {
            return $response;
        }
        unset($params);
        $params = [];
        $tmdbId = $response['results'][0]['id'];
        $endPoint = $this->buildEndpoint($params, 'movie/'. $tmdbId);
        $response = $this->getResponse($endPoint, self::IMDBID);

        $endPoint = $this->buildEndpoint($params, 'movie/'. $tmdbId. '/keywords');
        $response['keywords'] = $this->getResponse($endPoint, self::IMDBID);

        return $this->beautifyResponse($response);
    }

    /**
     * @param string $t
     * @return array
     */
    public function searchBySearching(string $s = 'the Matrix', $page = 1): array
    {
        $results = [];
        $pages = 1;
        $params['query'] = $s;
        $params['page'] = $page;
        $endPoint = $this->buildEndpoint($params, 'search/movie');
        $response = $this->getResponse($endPoint, self::SEARCHING);

        foreach ($response['results'] as $search) {
            $results[] = $this->beautifyResponse($search);
        }

        if (isset($response['total_results'])) {
            $pages = $response['total_results'] / 20;
        }

        if ($pages > 1) {
            // Pages
            for ($i = 2; $i <= $pages; $i++) {
                // Results per Page
                foreach ($response['Search'] as $search) {
                    $results[] = $this->beautifyResponse($search);
                }
                $params['page'] = $i;
                $endPoint = $this->buildEndpoint($params);
                $response = $this->getResponse($endPoint);
            }
        }
        return $results;
    }

}