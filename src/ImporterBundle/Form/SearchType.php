<?php

namespace ImporterBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SearchType
 * @package ImporterBundle\Form
 */
class SearchType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('search', TextType::class,
                [
                    'label' => false,

                    'attr' =>
                        [
                            'class' => 'form-control c-search__search',
                            'placeholder' => 'Search'

                        ]
                ]
            )
            ->add('context', ChoiceType::class,
                [
                    'choices' => $this->searchTypeAvaiable($options['searchTypesAvailable']),
                    'placeholder' => 'What?',
                    'label' => false,
                    'attr' =>
                        [
                            'class' => 'form-control c-search__context'
                        ]
                ])
        ->add('submit', SubmitType::class,
            [
                'label'=> 'Search',
                'attr' =>
                    [
                        'class' => 'btn btn-outline-info c-search__submit'
                    ]
            ]
        );
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ImporterBundle\Entity\Search',
            'searchTypesAvailable' => null,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'search';
    }
    private function searchTypeAvaiable($searches)
    {
        $choices = [];
        foreach($searches as $search)
        {
            $choices[str_replace(' ', '', $search)] = $search;
        }

        return $choices;
    }
}
