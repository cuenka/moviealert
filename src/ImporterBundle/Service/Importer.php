<?php

namespace ImporterBundle\Service;

use Doctrine\ORM\EntityManager;
use GuzzleHttp\Client;
use Imdb\Config;
use Imdb\PersonSearch;
use ImporterBundle\Entity\Search;
use MovieBundle\Entity\Cast;
use MovieBundle\Entity\Country;
use MovieBundle\Entity\Director;
use MovieBundle\Entity\Genre;
use MovieBundle\Entity\Keyword;
use MovieBundle\Entity\Language;
use MovieBundle\Entity\Movie;
use MovieBundle\Entity\Production;
use MovieBundle\Entity\Writer;
use MovieBundle\Helper\CountryHelper;
use MovieBundle\Helper\GenreHelper;
use MovieBundle\Helper\LanguageHelper;
use MovieBundle\Repository\CastRepository;
use MovieBundle\Repository\CountryRepository;
use MovieBundle\Repository\DirectorRepository;
use MovieBundle\Repository\GenreRepository;
use MovieBundle\Repository\KeywordRepository;
use MovieBundle\Repository\LanguageRepository;
use MovieBundle\Repository\MovieRepository;
use MovieBundle\Repository\ProductionRepository;
use MovieBundle\Repository\WriterRepository;
use Symfony\Component\DependencyInjection\Tests\Compiler\H;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Class Importer
 * @package ImporterBundle\Service
 */
class Importer
{
    /**
     * @var EntityManager
     */
    protected $em;
    /**
     * @var Config Config
     */
    protected $config;

    /**
     * @var string imagePath
     */
    protected $imagePath;

    /**
     * @var string moviedbKey
     */
    protected $moviedbKey;

    /**
     * Importer constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em, $imagePath, $moviedbKey= null)
    {
        $this->em = $em;
        $this->imagePath = realpath($imagePath);
        $this->config = new Config();
        $this->config->language = 'en-US,en,us,es-ES,es,ES';
        $this->moviedbKey = $moviedbKey;
    }

    /**
     * @return mixed
     */
    public function getInstanceFromSearches(string $instance)
    {
        return $this->em->getRepository()->getInstancesFromSearches();
    }

    /**
     *
     * @param string $instance
     * @return array
     */
    public function getInstances(string $instance)
    {
        return $this->em->getRepository($instance)->findAll();
    }

    /**
     * @return array|\ImporterBundle\Entity\Search[]
     */
    public function getGenre($genre)
    {
        return $this->em->getRepository('MovieBundle:Genre')->findOneBy(['name' => $genre]);
    }

    protected function findMovieTrailer($imdbID)
    {
        $paramsForID =
            [
                'api_key' => $this->moviedbKey,
                'language' => 'en-US',
                'external_source' => 'imdb_id'
            ];
        $paramsForTrailer =
            [
                'api_key' => $this->moviedbKey
            ];
        $urlForId = 'https://api.themoviedb.org/3/find/tt'. $imdbID. '?'. http_build_query($paramsForID);
        $client = new Client();
        $response = $client->request('GET', $urlForId);
        $id = json_decode($response->getBody(), true);
        $id = isset($id['movie_results'][0]['id']) ? $id['movie_results'][0]['id'] : 0;
        if ($id == 0) {
            return null;
        }
        $urlForTrailer = 'https://api.themoviedb.org/3/movie/'. $id. '/videos?'. http_build_query($paramsForTrailer);
        $response = $client->request('GET', $urlForTrailer);
        $trailer = json_decode($response->getBody(), true);
        unset($paramsForID);
        unset($paramsForTrailer);
        unset($urlForId);
        unset($client);
        unset($id);
        unset($urlForTrailer);
        return isset($trailer['results'][0]['key']) ? $trailer['results'][0]['key'] : null;
    }

    public function saveImage($url, $path)
    {
        $ch = curl_init($url);
        $fp = fopen($path, 'wb');
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $curlResult = curl_exec($ch);
        curl_close($ch);
        fclose($fp);

        if ($curlResult === true) {
            return true;
        }

        return false;
    }
    /**
     * @param Search $search
     */
    public function updateStatusSearch(Search $search)
    {
        $search->setProcessed(1);
        $search->setUpdated(new \DateTime());
        $this->em->persist($search);
        $this->em->flush();
    }

    /**
     * @param string $
     * @return mixed|string
     */
    public function slugger($title, $repository, $year = 1)
    {
        // Clean up
        $candidate = trim(str_replace(' ', '-', strtolower($title)));
        $candidate = preg_replace('/[^A-Za-z0-9\-]/', '', $candidate);
        $candidate = iconv('UTF-8', 'ISO-8859-1//TRANSLIT//IGNORE', $candidate);
        $i = 1;
        $result = $this->em->getRepository($repository)->findBySlug($candidate);
        // Movie exists, so we add Year
        if (count($result) > 0) {
            $candidate = $candidate. "-". $year;
            $result = $this->em->getRepository($repository)->findBySlug($candidate);
        }
        $defaultBase = $candidate;
        $base = $candidate;
        // Still no luck, We had numbers
        while(count($result) > 0) {
            $candidate = $base. "-". $i++;
            $result = $this->em->getRepository($repository)->findBySlug($candidate);
        }
        return $candidate;
    }

    /**
     * @param $search
     * @param $context
     */
    public function createNewSearch($search, $context)
    {
        $counts = $this->em->getRepository('ImporterBundle:Search')->getCountBysearchAndContext($search, $context);

        if ($counts == 0) {
            $newPotentialSearch = new Search();
            $newPotentialSearch->setContext($context)
                ->setPublished(new \DateTime())
                ->setUpdated(new \DateTime())
                ->setProcessed(false)
                ->setSearch($search);
            $this->em->persist($newPotentialSearch);
            $this->em->flush();
        }
        unset($newPotentialSearch);
        unset($counts);
    }

    public function getPersonBySearch($search, $imdbID = null, $context = null)
    {
        $personSearch = new PersonSearch($this->config);
        $results = $personSearch->search($search);
        foreach ($results as $result) {
            if ($result->imdbid() == $imdbID) {
                $candidate = $result;
            } else {
                $this->createNewSearch($result->name(), $context);
            }
        }
        unset($personSearch);
        unset($results);
        return (isset($candidate) ? $candidate : null);
    }
    /**
     * @param $date
     * @return mixed
     */
    protected function normaliseDate($date)
    {
        $day = intval((isset($date['day']) && !is_null($date['day'])) ? $date['day'] : null);
        $month = (isset($date['month']) && !is_null($date['month'])) ? $date['month'] : null;
        $year = intval((isset($date['year']) && !is_null($date['year'])) ? $date['year'] : null);
        if (is_null($day) || is_null($month)|| is_null($year)) {
            return null;
        }
        return \DateTime::createFromFormat('d M Y', $day.' '.$month.' '.$year);
    }

    /**
     * @param $text
     * @return string
     */
    protected function cleanUpText($text)
    {
        return strip_tags($text);

    }

    /**
     * @param $text
     * @return string
     */
    protected function cleanUpName($name)
    {
        return ucwords(trim($this->cleanUpText($name)));

    }

    /**
     * @param $repository
     * @param $name
     * @return bool
     */
    protected function existOnDBByName($repository, $name)
    {
        if ($this->em->getRepository($repository)->findOneByNameIfExist($name) == true) {
            return true;
        }

        return false;
    }

    /**
     * @param $repository
     * @param $imdbID
     * @return bool
     */
    protected function existOnDBByImdbdID($repository, $imdbID)
    {
        if ($this->em->getRepository($repository)->findByIMDBId($imdbID) == 1) {
            return true;
        }

        return false;
    }


    /**
     * @param string $name
     * @return Genre
     */
    public function newGenre(string $name): Genre
    {
        $name = $this->cleanUpName($name);
        $helperGenre = new GenreHelper();
        $name = $helperGenre->resolveSynonyms($name);
        $genre = new Genre();
        $genre->setSlug($this->slugger($name, GenreRepository::NAME))
            ->setName($name);

        $this->em->persist($genre);
        $this->em->flush();

        return $genre;
    }

    /**
     * @param string $search
     * @return Genre
     */
    public function importSingleGenre(string $search): Genre
    {
        $search = $this->cleanUpName($search);
        $helperGenre = new GenreHelper();
        $search = $helperGenre->resolveSynonyms($search);
        if ($this->existOnDBByName(GenreRepository::NAME, $search)) {
            $genre = $this->em->getRepository(GenreRepository::NAME)->findOneBy(['name' => $search]);
        } else {
            $genre = $this->newGenre($search);
        }

        return $genre;
    }

    /**
     * @param string $name
     * @return Keyword
     */
    public function newKeyword(string $name): Keyword
    {
        $name = $this->cleanUpName($name);

        $keyword = new Keyword();
        $keyword->setSlug($this->slugger($name, KeywordRepository::NAME))
            ->setName($name);

        $this->em->persist($keyword);
        $this->em->flush();

        return $keyword;
    }

    /**
     * @param string $search
     * @return Keyword
     */
    public function importSingleKeyword(string $search): Keyword
    {
        $search = $this->cleanUpName($search);
        if ($this->existOnDBByName(KeywordRepository::NAME, $search)) {
            $keyword = $this->em->getRepository(KeywordRepository::NAME)->findOneBy(['name' => $search]);
        } else {
            $keyword = $this->newKeyword($search);
        }

        return $keyword;
    }

    /**
     * @param string $name
     * @return Language
     */
    public function newLanguage(string $name): Language
    {
        $name = $this->cleanUpName($name);
        $helperLanguage = new LanguageHelper();
        $name = $helperLanguage->resolveSynonyms($name);
        $language = new Language();
        $language->setSlug($this->slugger($name, LanguageRepository::NAME))
            ->setName($name);

        return $language;
    }


    /**
     * @param string $search
     * @return Language
     */
    public function importSingleLanguage(string $search): Language
    {
        $search = $this->cleanUpName($search);
        $helperLanguage = new LanguageHelper();
        $search = $helperLanguage->resolveSynonyms($search);
        if ($this->existOnDBByName(LanguageRepository::NAME,  $search)) {
            $language = $this->em->getRepository(LanguageRepository::NAME)->findOneBy(['name' => $search]);
        } else {
            $language = $this->newLanguage($search);
        }

        return $language;
    }

    /**
     * @param string $name
     * @return Country
     */
    public function newCountry(string $name): Country
    {
        $name = $this->cleanUpName($name);
        $helperCountry = new CountryHelper();
        $name = $helperCountry->resolveSynonyms($name);
        $country = new Country();
        $country->setName($name);
        $country->setIsoCode($helperCountry->getIsoCode($name));

        return $country;
    }


    /**
     * @param string $search
     * @return Country
     */
    public function importSingleCountry(string $search): Country
    {
        $search = $this->cleanUpName($search);
        $helperCountry = new CountryHelper();
        $search = $helperCountry->resolveSynonyms($search);
        if ($this->existOnDBByName(CountryRepository::NAME,  $search)) {
            $country = $this->em->getRepository(CountryRepository::NAME)->findOneBy(['name' => $search]);
        } else {
            $country = $this->newCountry($search);
        }

        return $country;
    }

    /**
     * @param string $name
     * @return Director
     */
    public function newDirector(string $name): Director
    {
        $name = $this->cleanUpName($name);
        $director = new Director();
        $director->setSlug($this->slugger($name, DirectorRepository::NAME))
            ->setName($name);

        return $director;
    }


    /**
     * @param string $search
     * @return Director
     */
    public function importSingleDirector(string $search): Director
    {
        $search = $this->cleanUpName($search);
        if ($this->existOnDBByName(DirectorRepository::NAME, $search)) {
            $director = $this->em->getRepository(DirectorRepository::NAME)->findOneBy(['name' => $search]);
        } else {
            $director = $this->newDirector($search);
        }

        return $director;
    }


    /**
     * @param string $name
     * @return Writer
     */
    public function newWriter(string $name): Writer
    {
        $name = $this->cleanUpName($name);
        $writer = new Writer();
        $writer->setSlug($this->slugger($name, WriterRepository::NAME))
            ->setName($name);

        return $writer;
    }

    /**
     * @param string $search
     * @return Writer
     */
    public function importSingleWriter(string $search): Writer
    {
        $search = $this->cleanUpName($search);
        if ($this->existOnDBByName(WriterRepository::NAME, $search)) {
            $writer = $this->em->getRepository(WriterRepository::NAME)->findOneBy(['name' => $search]);
        } else {
            $writer = $this->newWriter($search);
        }

        return $writer;
    }


    /**
     * @param string $name
     * @return Production
     */
    public function newProduction(string $name): Production
    {
        $name = $this->cleanUpName($name);
        $production = new Production();
        $production->setName($name);
        return $production;
    }

    /**
     * @param string $search
     * @return Production
     */
    public function importSingleProduction(string $search): Production
    {
        $search = $this->cleanUpName($search);
        if ($this->existOnDBByName(ProductionRepository::NAME, $search)) {
            $production = $this->em->getRepository(ProductionRepository::NAME)->findOneBy(['name' => $search]);
        } else {
            $production = $this->newProduction($search);
        }

        return $production;
    }

    /**
     * @param string $name
     * @return Cast
     */
    public function newCast(string $name): Cast
    {
        $name = $this->cleanUpName($name);
        $cast = new Cast();
        $cast->setSlug($this->slugger($name, CastRepository::NAME))
            ->setName($name);

        return $cast;
    }

    /**
     * @param string $search
     * @return W
     */
    public function importSingleCast(string $search): Cast
    {
        $search = $this->cleanUpName($search);
        if ($this->existOnDBByName(CastRepository::NAME, $search)) {
            $cast = $this->em->getRepository(CastRepository::NAME)->findOneBy(['name' => $search]);
        } else {
            $cast = $this->newCast($search);
        }

        return $cast;
    }



}