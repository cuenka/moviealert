<?php

namespace ImporterBundle\Service;

use Imdb\Config;
use Imdb\PersonSearch;
use Imdb\Title;
use Imdb\TitleSearch;
use ImporterBundle\Entity\Search;
use MovieBundle\Entity\Genre;
use MovieBundle\Entity\Director;
use MovieBundle\Entity\Movie;


/**
 * Class MovieImporter
 * @package ImporterBundle\Service
 */
class GenreImporter extends Importer
{
    /**
     * @return mixed
     */
    public function getGenresFromSearches()
    {

        return $this->em->getRepository('ImporterBundle:Search')->getGenresFromSearches();
    }

    /**
     * @return array|\ImporterBundle\Entity\Search[]
     */
    public function getGenres()
    {
        return $this->em->getRepository('MovieBundle:Genre')->findAll();
    }

    /**
     * @return array|\ImporterBundle\Entity\Search[]
     */
    public function getGenre($genre)
    {
        return $this->em->getRepository('MovieBundle:Genre')->findOneBy(['name' => $this->cleanUpName($genre)]);
    }


}