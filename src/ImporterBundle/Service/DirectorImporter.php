<?php

namespace ImporterBundle\Service;

use Imdb\Config;
use Imdb\PersonSearch;
use Imdb\Title;
use Imdb\TitleSearch;
use ImporterBundle\Api\ImdbApi;
use ImporterBundle\Entity\Search;
use MovieBundle\Entity\Director;
use MovieBundle\Entity\Movie;
use MovieBundle\Repository\DirectorRepository;


/**
 * Class MovieImporter
 * @package ImporterBundle\Service
 */
class DirectorImporter extends Importer
{

    /**
     * @return mixed
     */
    public function getDirectors()
    {
        return $this->em->getRepository('ImporterBundle:Search')->getDirectorsFromSearches();
    }


    /**
     * @return mixed
     */
    public function getComposersForReview()
    {
        return $this->em->getRepository(DirectorRepository::NAME)->findForReview();
    }

    /**
     * @param $info
     * @return Director
     */
    public function newDirector2($potentialDirector)
    {
        $director = new Director();
        $person = $this->getPersonBySearch($potentialDirector['name'], $potentialDirector['imdb'], Search::CONTEXT_DIRECTOR);
        $director->setSlug($this->slugger($person->name(), 'MovieBundle:Director'))
            ->setName($person->name())
            ->setImdbId($person->imdbid());
        $biography = (isset($person->bio()[0]['desc']) ? $person->bio()[0]['desc'] : null);
        $biography = $this->cleanUpText($biography);
        $director->setBiography($biography);
        $director->setAge($this->normaliseDate($person->born()));
        $this->em->persist($director);
        $this->em->flush();
        if ($person->savephoto(
            $this->imagePath. DIRECTORY_SEPARATOR. Search::CONTEXT_DIRECTOR. DIRECTORY_SEPARATOR. $director->getId(). '.jpg',
            false,
            true)) {
            $director->setThumbnail(true);
            $this->em->persist($director);
            $this->em->flush();
        }
        return $director;
    }

    public function importSingleDirector2(Search $search, $scanFilmography = false)
    {

        $personSearch = new PersonSearch($this->config);
        $results = $personSearch->search($search->getSearch());


        foreach($results as $result) {
            if (count($result->movies_director()) > 0) {
                    $isOnDB = $this->em->getRepository("MovieBundle:Director")->findByIMDBId(
                        $result->imdbid()
                    );

                    if ($isOnDB == 0) {
                        $director = new Director();
                        $director->setImdbId($result->imdbid());
                        $director->setPublished(new \DateTime());
                        $director->setModified(new \DateTime());
                        $director->setBirthday($this->normaliseDate($result->born()));
                        $director->setName($result->name());
                        $biography = (isset($result->bio()[0]['desc']) ? $result->bio()[0]['desc'] : null);
                        $biography = $this->cleanUpText($biography);
                        $director->setBiography($biography);
                        $result->born();
                        $slug = $this->slugger($director->getName(), 'MovieBundle:Director', isset($result->born()['year'])? $result->born()['year']: 1870);
                        $director->setSlug($slug);
                        $this->em->persist($director);
                        $this->em->flush();
                        if ($result->savephoto(
                            $this->imagePath. DIRECTORY_SEPARATOR. Search::CONTEXT_DIRECTOR. DIRECTORY_SEPARATOR. $director->getId().'.jpg',
                            false,
                            true)) {
                            $director->setThumbnail(true);
                            $this->em->persist($director);
                            $this->em->flush();
                        }

                    }

                    if ($scanFilmography == true) {
                        $potentialFilms = $result->movies_all();

                        foreach ($potentialFilms as $potentialFilm) {
                            if ($potentialFilm['title_type'] == 'Movie') {
                                $newPotentialFilm = new Search();
                                $newPotentialFilm->setContext(Search::CONTEXT_MOVIE)
                                    ->setPublished(new \DateTime())
                                    ->setUpdated(new \DateTime())
                                    ->setProcessed(false)
                                    ->setSearch($potentialFilm['name']);
                                $this->em->persist($newPotentialFilm);
                                $this->em->flush();
                            }
                        }
                }
//                }
            }
        }
    }

    /**
     * @param Director $cast
     */
    public function reviewInstance(Director $director)
    {
        $imdb = new ImdbApi();
        $person = $imdb->searchOnePersonByName($director->getName(), $director->getImdbId());

        if (is_null($director->getBiography())) {
            $director->setBiography($person['biography']);
        }

        if (is_null($director->getBirthday())) {
            $director->setBirthday($this->normaliseDate($person['birthday']));
        }

        if (!is_null($person['thumbnail']) && ($director->getThumbnail() === false)) {
            $imagePath = $this->imagePath.DIRECTORY_SEPARATOR.'director'.DIRECTORY_SEPARATOR.$director->getId().'.jpg';
            $isSaved = $this->saveImage($person['thumbnail'], $imagePath);
            if ($isSaved) {
                $director->setThumbnail(true);
            }
        }
        if (!empty($person['filmography'])) {
            foreach ($person['filmography'] as $movie) {
                $movieImdbId = 'tt'.$movie['mid'];
                $movieName = empty($movie['name']) ? null : $movie['name'];
                $movieYear = empty(intval($movie['year'])) ? null : intval($movie['year']);
                $this->createNewSearch($movieName, Search::CONTEXT_MOVIE);
                $this->em->flush();
            }
        }
        $director->setNeedReview(false);
        $director->setModified(new \DateTime());
        $this->em->persist($director);
        $this->em->flush();

        return 'Director '. $director->getName() .', updated successfully';
    }
    public function importSinglePerson(Search $search)
    {

        $personSearch = new PersonSearch($this->config);
        $results = $personSearch->search($search->getSearch());

        foreach($results as $result) {
            if ($result->movies_director()) {
                $isOnDB = $this->em->getRepository("MovieBundle:Director")->findByIMDBId($result->imdbid());
                if ($isOnDB == 0) {

                }
            }
        }
    }

}