<?php

namespace ImporterBundle\Service;

use Imdb\Config;
use Imdb\PersonSearch;
use Imdb\Title;
use Imdb\TitleSearch;
use ImporterBundle\Api\ImdbApi;
use ImporterBundle\Entity\Search;
use MovieBundle\Entity\Cast;
use MovieBundle\Entity\Character;
use MovieBundle\Entity\Movie;
use MovieBundle\Repository\CastRepository;
use MovieBundle\Repository\CharacterRepository;
use MovieBundle\Repository\MovieRepository;


/**
 * Class MovieImporter
 * @package ImporterBundle\Service
 */
class CastImporter extends Importer
{

    /**
     * @return mixed
     */
    public function getCast()
    {
        return $this->em->getRepository('ImporterBundle:Search')->getCastsFromSearches();
    }

    /**
     * @return mixed
     */
    public function getCastForReview()
    {
        return $this->em->getRepository(CastRepository::NAME)->findForReview();
    }

    /**
     * @return mixed
     */
    public function getCharacter(Movie $movie, Cast $cast)
    {
        return $this->em->getRepository(CharacterRepository::NAME)
            ->findOneBy(
                [
                    'movie' => $movie,
                    'cast'  => $cast
                ]
            );
    }

    /**
     * @param Cast $cast
     */
    public function reviewInstance(Cast $cast)
    {
        $imdb = new ImdbApi();
        $person = $imdb->searchOnePersonByName($cast->getName(), $cast->getImdbId(), ['Actor', 'Actress']);

        if(is_null($cast->getImdbId())) {
            $cast->setImdbId($person['imdbId']);
        }

        if(is_null($cast->getBiography())) {
            $cast->setBiography($person['biography']);
        }

        if(is_null($cast->getBirthday())) {
            $cast->setBirthday($this->normaliseDate($person['birthday']));
        }

        if (!is_null($person['thumbnail']) && ($cast->getThumbnail() === false)) {
            $imagePath =  $this->imagePath . DIRECTORY_SEPARATOR. 'cast'. DIRECTORY_SEPARATOR. $cast->getId(). '.jpg';
            $isSaved = $this->saveImage($person['thumbnail'], $imagePath);
            if ($isSaved) {
                $cast->setThumbnail(true);
            }
        }

        if (!empty($person['filmography'])) {
            foreach ($person['filmography'] as $movie) {
                $movieImdbId = 'tt'. $movie['mid'];
                $movieName = empty($movie['name']) ? null : $movie['name'];
                $movieYear = empty(intval($movie['year'])) ? null : intval($movie['year']);
                $this->createNewSearch($movieName, Search::CONTEXT_MOVIE);
//                $instanceMovie = $this
//                    ->importSingleMovie($movieName);
//
//                if ($instanceMovie->checkCastAdded($cast) === false) {
//                    $character = new Character();
//                    $character->setCast($cast)
//                        ->setMovie($instanceMovie)
//                        ->setRole($movie['chname']);
//                    $this->em->persist($instanceMovie);
//                    $this->em->persist($cast);
//                } else {
//                    $character = $this->getCharacter($instanceMovie, $cast);
//                    if ($character->getRole() == null) {
//                        $character->setRole($movie['chname']);
//                        $character->setModified(new \DateTime());
//                        $this->em->persist($character);
//                    }
//                }
                $this->em->flush();
            }
        }

        $cast->setNeedReview(false);
        $this->em->persist($cast);
        $this->em->flush();
        return 'Updated: '. $cast->getName();
    }
    /**
     * @param $info
     * @return Cast
     */
    public function newCast2($potentialCast)
    {
        $cast = new Cast();
        $person = $this->getPersonBySearch($potentialCast['name'], $potentialCast['imdb'], Search::CONTEXT_CAST);
        $cast->setSlug($this->slugger($person->name(), 'MovieBundle:Cast'))
            ->setName($person->name())
            ->setImdbId($person->imdbid());
        $biography = (isset($person->bio()[0]['desc']) ? $person->bio()[0]['desc'] : null);
        $biography = $this->cleanUpText($biography);
        $cast->setBiography($biography);

        $cast->setBirthday($this->normaliseDate($person->born()));
        $this->em->persist($cast);
        $this->em->flush();
        if ($person->savephoto(
            $this->imagePath.DIRECTORY_SEPARATOR.Search::CONTEXT_CAST.DIRECTORY_SEPARATOR.$cast->getId().'.jpg',
            false,
            true
        )
        ) {
            $cast->setThumbnail(true);
            $this->em->persist($cast);
            $this->em->flush();
        }
        $person = null;
        $biography = null;

        return $cast;
    }

    public function importSingleCast2(Search $search, $scanFilmography = false)
    {

        $personSearch = new PersonSearch($this->config);
        $results = $personSearch->search($search->getSearch());


        foreach ($results as $result) {
            if (count($result->movies_actor()) > 0) {
                $isOnDB = $this->em->getRepository("MovieBundle:Cast")->findByIMDBId(
                    $result->imdbid()
                );

                if ($isOnDB == 0) {
                    $cast = new Cast();
                    $cast->setImdbId($result->imdbid());
                    $cast->setBirthday($this->normaliseDate($result->born()));
                    $cast->setName($result->name());
                    $biography = (isset($result->bio()[0]['desc']) ? $result->bio()[0]['desc'] : null);
                    $biography = $this->cleanUpText($biography);
                    $cast->setBiography($biography);
                    $result->born();
                    $slug = $this->slugger(
                        $cast->getName(),
                        'MovieBundle:Cast',
                        isset($result->born()['year']) ? $result->born()['year'] : 1870
                    );
                    $cast->setSlug($slug);
                    $this->em->persist($cast);
                    $this->em->flush();
                    if ($result->savephoto(
                        $this->imagePath. DIRECTORY_SEPARATOR .Search::CONTEXT_CAST.DIRECTORY_SEPARATOR.$cast->getId().'.jpg',
                        false,
                        true
                    )
                    ) {
                        $cast->setThumbnail(true);
                        $this->em->persist($cast);
                        $this->em->flush();
                    }
                    if ($scanFilmography === true) {
                        $filmography = $result->movies_all();
                        foreach($filmography as $film)
                        if ($this->existOnDBByImdbdID('MovieBundle:Movie', $film['mid'])) {
                            $instanceMovie = $this->em->getRepository('MovieBundle:Movie')
                                ->findOneBy(['imdbId' => $film['mid']]);
                            $character = new Character();
                            $character->setCast($cast)
                                ->setMovie($instanceMovie)
                                ->setRole($film['chname']);
                            $this->em->persist($character);
                            $this->em->flush();

                        } else {
                            $instanceMovie = new Movie();
                            $instanceMovie->setTitle($film['name'])
                                ->setImdbId($film['mid']);
                            $slug = $this->slugger($film['name'], 'MovieBundle:Movie', $film['year']);
                                $instanceMovie->setSlug($slug);
                            $instanceMovie->setYear(intval($film['year']));

                            $character = new Character();
                            $character->setCast($cast)
                                ->setMovie($instanceMovie)
                                ->setRole($film['chname']);
                            $this->em->persist($instanceMovie);
                            $this->em->persist($character);
                            $this->em->flush();
                        }
                    }
                }
            }
            echo "-----Saved: '".$result->name(). "' ". memory_get_usage()."\n";
            unset($result);
        }
    }

    public function importSinglePerson(Search $search)
    {

        $personSearch = new PersonSearch($this->config);
        $results = $personSearch->search($search->getSearch());

        foreach ($results as $result) {
            if ($result->movies_cast()) {
                $isOnDB = $this->em->getRepository("MovieBundle:Cast")->findByIMDBId($result->imdbid());
                if ($isOnDB == 0) {

                }
            }
        }
    }

}