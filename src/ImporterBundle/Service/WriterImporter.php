<?php

namespace ImporterBundle\Service;

use Imdb\Config;
use Imdb\PersonSearch;
use Imdb\Title;
use Imdb\TitleSearch;
use ImporterBundle\Api\ImdbApi;
use ImporterBundle\Entity\Search;
use MovieBundle\Entity\Writer;
use MovieBundle\Entity\Director;
use MovieBundle\Entity\Movie;
use MovieBundle\Repository\WriterRepository;


/**
 * Class MovieImporter
 * @package ImporterBundle\Service
 */
class WriterImporter extends Importer
{

    public function getWriters()
    {
        return $this->em->getRepository('ImporterBundle:Search')->getWritersFromSearches();
    }

    /**
     * @return mixed
     */
    public function getWritersForReview()
    {
        return $this->em->getRepository(WriterRepository::NAME)->findForReview();
    }

    /**
     * @param Writer $writer
     */
    public function reviewInstance(Writer $writer)
    {
        $imdb = new ImdbApi();
        $person = $imdb->searchOnePersonByName($writer->getName(), $writer->getImdbId(), 'writer');

        if(is_null($writer->getBiography())) {
            $writer->setBiography($person['biography']);
        }

        if(is_null($writer->getBirthday())) {
            $writer->setBirthday($this->normaliseDate($person['birthday']));
        }

        if (!is_null($person['thumbnail']) && ($writer->getThumbnail() === false)) {
            $imagePath =  $this->imagePath . DIRECTORY_SEPARATOR. 'writer'. DIRECTORY_SEPARATOR. $writer->getId(). '.jpg';
            $isSaved = $this->saveImage($person['thumbnail'], $imagePath);
            if ($isSaved) {
                $writer->setThumbnail(true);
            }
        }

        if (!empty($person['filmography'])) {
            foreach ($person['filmography'] as $movie) {
                $movieImdbId = 'tt'. $movie['mid'];
                $movieName = empty($movie['name']) ? null : $movie['name'];
                $movieYear = empty(intval($movie['year'])) ? null : intval($movie['year']);
                $this->createNewSearch($movieName, Search::CONTEXT_MOVIE);
                $this->em->flush();
            }
        }

        $writer->setNeedReview(false);
        $this->em->persist($writer);
        $this->em->flush();
        return 'Updated: '. $writer->getName();
    }
    

    /**
     * @param $info
     * @return Director
     */
    public function newWriter2($potentialWriter)
    {
        $writer = new Writer();
        $person = $this->getPersonBySearch($potentialWriter['name'], $potentialWriter['imdb'], Search::CONTEXT_WRITER);
        $writer->setSlug($this->slugger($person->name(), 'MovieBundle:Writer'))
            ->setName($person->name())
            ->setImdbId($person->imdbid());
        $biography = (isset($person->bio()[0]['desc']) ? $person->bio()[0]['desc'] : null);
        $biography = $this->cleanUpText($biography);
        $writer->setBiography($biography);
        $writer->setBirthday($this->normaliseDate($person->born()));
        $this->em->persist($writer);
        $this->em->flush();
        if ($person->savephoto(
            $this->imagePath. DIRECTORY_SEPARATOR. Search::CONTEXT_WRITER. DIRECTORY_SEPARATOR. $writer->getId(). '.jpg',
            false,
            true)) {
            $writer->setThumbnail(true);
            $this->em->persist($writer);
            $this->em->flush();
        }
        return $writer;
    }
}