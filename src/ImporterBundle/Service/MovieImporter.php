<?php

namespace ImporterBundle\Service;

use Doctrine\ORM\EntityManager;
use Imdb\Config;
use Imdb\Title;
use Imdb\TitleSearch;
use ImporterBundle\Api\OmdbApi;
use ImporterBundle\Api\TmdbApi;
use ImporterBundle\Entity\Search;
use MovieBundle\Entity\Character;
use MovieBundle\Entity\Composer;
use MovieBundle\Entity\Genre;
use MovieBundle\Entity\Movie;
use MovieBundle\Entity\Production;
use MovieBundle\Repository\MovieRepository;


/**
 * Class MovieImporter
 * @package ImporterBundle\Service
 */
class MovieImporter extends Importer
{
    /**
     * @var GenreImporter
     */
    private $serviceGenre;

    /**
     * @var DirectorImporter
     */
    private $serviceDirector;

    /**
     * @var CastImporter
     */
    private $serviceCast;

    /**
     * @var serviceComposer
     */
    private $serviceComposer;

    /**
     * @var serviceWriter
     */
    private $serviceWriter;

    /**
     * @var serviceProducer
     */
    private $serviceProducer;

    /**
     * Importer constructor.
     * @param EntityManager $em
     */
    public function __construct(
        EntityManager $em,
        $imagePath,
        $moviedbKey,
        GenreImporter $serviceGenre,
        DirectorImporter $serviceDirector,
        CastImporter $serviceCast,
        ComposerImporter $serviceComposer,
        WriterImporter $serviceWriter,
        ProducerImporter $serviceProducer
    ) {
        parent::__construct($em, $imagePath, $moviedbKey);
        $this->serviceGenre = $serviceGenre;
        $this->serviceDirector = $serviceDirector;
        $this->serviceCast = $serviceCast;
        $this->serviceComposer = $serviceComposer;
        $this->serviceWriter = $serviceWriter;
        $this->serviceProducer = $serviceProducer;
    }

    /**
     * @return mixed
     */
    public function getMovies()
    {
        return $this->em->getRepository('ImporterBundle:Search')->getMoviesFromSearches();
    }

    /**
     * @return mixed
     */
    public function getAllMovies($numMovies = null, $startingId = null)
    {
        if (is_null($numMovies) || is_null($startingId)) {
            return $this->em->getRepository('MovieBundle:Movie')->findAll();
        } else {
            return $this->em->getRepository('MovieBundle:Movie')
                ->findbyIdWithLimit($numMovies, $startingId);
        }
    }

    /**
     * @return mixed
     */
    public function getMoviesForReview()
    {
        return $this->em->getRepository(MovieRepository::NAME)->findForReview();
    }

    /**
     * @param Search $search
     * @return string
     */
    public function importSingleMovieBySearch(Search $search)
    {
        $candidate =   str_replace(' ', '%20', preg_replace('/[^ \w]+/', '', $search->getSearch()));

        $tmdb = new TmdbApi();
        $data1 = $tmdb->searchByTitle($search->getSearch());
        $omdb = new OmdbApi();
        $data2 = $omdb->searchByTitle($search->getSearch());
        $result = $omdb->mergeResponses($data1, $data2);

        if (isset($result['error'])) {
            return $result['error'];
        }

        if (is_null($result['imdbID'])) {
            return 'Movie found, but not IMDBid, so ignored';
        }

        if ($this->existOnDBByImdbdID(MovieRepository::NAME, $result['imdbID'])) {
            return "Film on DB already found";

        }
        $movie = new Movie();
        sleep(4);
        return $this->updateMovie($movie, $result);
    }

    /**
     * @param Movie $movie
     * @return string
     */
    public function reviewInstance(Movie $movie)
    {
        $tmdb = new TmdbApi();
        $dataReference1 = $tmdb->searchByImdbID($movie->getImdbId());
        $omdb = new OmdbApi();
        $dataReference2 = $omdb->searchByImdbID($movie->getImdbId());
        $result = $omdb->mergeResponses($dataReference1, $dataReference2);
        sleep(4);
        if (isset($result['error'])) {
            $movie->setNeedReview(false);
            $movie->setUpdated(new \DateTime());
            $this->em->persist($movie);
            $this->em->flush();
            return $result['error'];
        }
        return $this->updateMovie($movie, $result);
    }

    /**
     * @param Movie $movie
     * @param array $result
     * @return string
     */
    private function updateMovie(Movie $movie, array $result)
    {
        $movie->setData($result);

        // Check if It is adult
        if ($result['adult'] == true) {
            $this->em->remove($movie);
            $this->em->flush();
            return 'Removed Adult Film';
        }

        // Genres
        if (isset($result['genre'])) {
            foreach ($result['genre'] as $genre) {
                $instanceGenre = $this->importSingleGenre($genre);
                // Check if relationship already exists
                if ($movie->checkGenreAdded($instanceGenre) === false) {
                    $movie->addGenre($instanceGenre);
                    $this->em->persist($instanceGenre);

                }
            }
        }

        // keywords
        if (isset($result['keywords'])) {
            foreach ($result['keywords'] as $keyword) {
                $instanceKeyword = $this->importSingleKeyword($this->cleanUpName($keyword));
                // Check if relationship already exists
                if ($movie->checkKeywordAdded($instanceKeyword) === false) {
                    $movie->addKeyword($instanceKeyword);
                    $this->em->persist($instanceKeyword);

                }
            }
        }

        // Languages
        if (isset($result['language'])) {
            foreach ($result['language'] as $language) {
                $instanceLanguage = $this->importSingleLanguage($language);
                if ($movie->checkLanguageAdded($instanceLanguage) === false) {
                    $movie->addLanguage($instanceLanguage);
                    $this->em->persist($instanceLanguage);

                }
            }
        }

        // Countries
        if (isset($result['country'])) {
            foreach ($result['country'] as $country) {
                $instanceCountry = $this->importSingleCountry($country);
                if ($movie->checkCountryAdded($instanceCountry) === false) {
                    $movie->addCountry($instanceCountry);
                    $this->em->persist($instanceCountry);
                }
            }
        }
        // Director
        if (isset($result['director'])) {
            foreach ($result['director'] as $director) {
                $instanceDirector = $this->importSingleDirector($director);
                if ($movie->checkDirectorAdded($instanceDirector) === false) {
                    $movie->addDirector($instanceDirector);
                    $this->em->persist($instanceDirector);
                }
            }
        }

        // Writer
        if (isset($result['writer'])) {
            foreach ($result['writer'] as $writer) {
                $instanceWriter = $this->importSingleWriter($writer);
                if ($movie->checkWriterAdded($instanceWriter) === false) {
                    $movie->addWriter($instanceWriter);
                    $this->em->persist($instanceWriter);
                }
            }
        }

        // Cast
        if (isset($result['cast'])) {
            foreach ($result['cast'] as $cast) {
                $instanceCast = $this->importSingleCast($cast);
                if ($movie->checkCastAdded($instanceCast) === false) {
                    $character = new Character();
                    $character->setCast($instanceCast)
                        ->setMovie($movie);
                    $this->em->persist($instanceCast);
                    $this->em->persist($character);
                }
            }
        }

        // Production
        if (isset($result['production'])) {
            foreach ($result['production'] as $production) {
                $instanceProduction = $this->importSingleProduction($production);
                if ($movie->checkProductionAdded($instanceProduction) === false) {
                    $movie->addProduction($instanceProduction);
                    $this->em->persist($instanceProduction);
                }
            }
        }
        if (is_null($movie->getSlug())){
            $movie->setSlug($this->slugger($movie->getTitle(), MovieRepository::NAME, $movie->getYear()));
        }
        $this->em->persist($movie);
        $this->em->flush();

        if (isset($result['poster']) && ($movie->getPoster() === false)) {
            $imagePath =  $this->imagePath . DIRECTORY_SEPARATOR. 'movie'. DIRECTORY_SEPARATOR. $movie->getId(). '.jpg';
            $isSaved = $this->saveImage($result['poster'], $imagePath);
            if ($isSaved) {
                $movie->setPoster(true);
            }
        }

        if (isset($result['backgroundPoster']) && ($movie->getScreenshot() === false)) {
            $imagePath =  $this->imagePath . DIRECTORY_SEPARATOR. 'screenshot'. DIRECTORY_SEPARATOR. 'movie'.
                DIRECTORY_SEPARATOR. $movie->getId(). '.jpg';
            $isSaved = $this->saveImage($result['backgroundPoster'], $imagePath);
            if ($isSaved) {
                $movie->setScreenshot(true);
            }
        }

        $movie->setNeedReview(false);
        $movie->setUpdated(new \DateTime());
        $this->em->persist($movie);
        $this->em->flush();

        return 'Movie updated successfully';
    }

    /**
     * @param Movie $movie
     * @param array $countries
     */
    private function getCountries(Movie &$movie, array &$countries)
    {
        foreach ($countries as $country) {
            $entityCountry = $this->em->getRepository("MovieBundle:Country")->findOrAddCountry($country);
            $movie->addCountry($entityCountry);
            unset($entityCountry);
        }
    }

    /**
     * @param Movie $movie
     * @param array $genres
     * @return Movie
     */
    private function getGenres(Movie &$movie, array &$genres)
    {
        foreach ($genres as $genre) {
            if ($this->existOnDBByName('MovieBundle:Genre', $genre)) {
                $instanceGenre = $this->em->getRepository('MovieBundle:Genre')->findOneBy(['name' => $genre]);
            } else {
                $instanceGenre = $this->serviceGenre->newGenre($genre);
            }

            $movie->addGenre($instanceGenre);
            $instanceGenre->addFilm($movie);
            $this->em->persist($instanceGenre);
        }
        $instanceGenre = null;
    }

    /**
     * @param Movie $movie
     * @param array $directors
     * @return mixed
     */
    private function getDirectors(Movie $movie, array $directors)
    {
        foreach ($directors as $director) {
            if ($this->existOnDBByImdbdID('MovieBundle:Director', $director['imdb'])) {
                $instanceDirector = $this->em->getRepository('MovieBundle:Director')
                    ->findOneBy(['imdbId' => $director['imdb']]);
            } else {
                $instanceDirector = $this->serviceDirector->newDirector($director);
            }

            if (!$movie->checkDirectorAdded($instanceDirector)) {
                $movie->addDirector($instanceDirector);
                $instanceDirector->addFilm($movie);
                $this->em->persist($instanceDirector);
            }
        }
        unset($instanceDirector);

        return $movie;
    }

    /**
     * @param Movie $movie
     * @param array $directors
     * @return mixed
     */
    private function getDirectorsForOMDB(Movie $movie, array $directors)
    {
        foreach ($directors as $director) {
            $search = new Search();
            $search->setContext(Search::CONTEXT_DIRECTOR)
                ->setSearch($director);
            $this->em->persist($search);
        }
        $this->em->flush();

        return $movie;
    }


    /**
     * @param Movie $movie
     * @param array $producers
     * @return mixed
     */
    private function getProducers(Movie &$movie, array &$producers)
    {
        foreach ($producers as $producer) {
            if ($this->existOnDBByImdbdID('MovieBundle:Producer', $producer['imdb'])) {
                $instanceProducer = $this->em->getRepository('MovieBundle:Producer')
                    ->findOneBy(['imdbId' => $producer['imdb']]);
            } else {
                $instanceProducer = $this->serviceProducer->newProducer($producer);
            }

            if (!$movie->checkProducerAdded($instanceProducer)) {
                $instanceProduction = new Production();
                $instanceProduction->setProducer($instanceProducer)
                    ->setMovie($movie)
                    ->setRole($producer['role']);
                $movie->addProduction($instanceProduction);
                $this->em->persist($instanceProduction);
                $this->em->flush();
            }
        }

        $instanceProduction = null;
    }

    /**
     * @param Movie $movie
     * @param array $producers
     * @return mixed
     */
    private function getProducersForOMDB(Movie &$movie, array &$producers)
    {
        foreach ($producers as $producer) {
            $search = new Search();
            $search->setContext(Search::CONTEXT_PRODUCER)
                ->setSearch($producer);
            $this->em->persist($search);
        }
        $this->em->flush();

        return $movie;
    }


    /**
     * @param Movie $movie
     * @param array $characters
     * @return mixed
     */
    private function getCharactersForOMDB(Movie &$movie, array &$characters)
    {
        foreach ($characters as $character) {
            $search = new Search();
            $search->setContext(Search::CONTEXT_CAST)
                ->setSearch($character);
            $this->em->persist($search);
        }
        $this->em->flush();

        return $movie;
    }

    /**
     * @param Movie $movie
     * @param array $characters
     * @return mixed
     */
    private function getCharacters(Movie &$movie, array &$characters)
    {
        foreach ($characters as $character) {
            if ($this->existOnDBByImdbdID('MovieBundle:Cast', $character['imdb'])) {
                $instanceCast = $this->em->getRepository('MovieBundle:Cast')->findOneBy(
                    ['imdbId' => $character['imdb']]
                );
            } else {
                $instanceCast = $this->serviceCast->newCast($character);
            }
            if (!$movie->checkCastAdded($instanceCast)) {
                $instanceCharacter = new Character();
                $instanceCharacter->setCast($instanceCast)
                    ->setMovie($movie)
                    ->setRole($character['role']);
                $movie->addCharacter($instanceCharacter);
                $this->em->persist($instanceCharacter);
                $this->em->flush();
            }
        }

        $instanceCharacter = null;
        $instanceCast = null;
    }

    /**
     * @param Movie $movie
     * @param array $composers
     * @return mixed
     */
    private function getComposer(Movie &$movie, array &$composers)
    {
        foreach ($composers as $composer) {
            if ($this->existOnDBByImdbdID('MovieBundle:Composer', $composer['imdb'])) {
                $instanceComposer = $this->em->getRepository('MovieBundle:Composer')->findOneBy(
                    ['imdbId' => $composer['imdb']]
                );
            } else {
                $instanceComposer = $this->serviceComposer->newComposer($composer);
            }
            if (!$movie->checkComposerAdded($instanceComposer)) {
                $movie->addComposer($instanceComposer);
                $this->em->persist($instanceComposer);
                $this->em->flush();
            }
        }
        $instanceComposer = null;
    }

    /**
     * @param Movie $movie
     * @param array $characters
     * @return mixed
     */
    private function getComposerForOMDB(Movie &$movie, array &$composers)
    {
        foreach ($composers as $composer) {
            $search = new Search();
            $search->setContext(Search::CONTEXT_CAST)
                ->setSearch($composer);
            $this->em->persist($search);
        }
        $this->em->flush();

        return $movie;
    }

    /**
     * @param Movie $movie
     * @param array $characters
     * @return mixed
     */
    private function getWriterForOMDB(Movie &$movie, array &$writers)
    {
        foreach ($writers as $writer) {
            $search = new Search();
            $search->setContext(Search::CONTEXT_CAST)
                ->setSearch($writer);
            $this->em->persist($search);
        }
        $this->em->flush();

        return $movie;
    }

    /**
     * @param Movie $movie
     * @param array $writers
     * @return mixed
     */
    private function getWriter(Movie &$movie, array &$writers)
    {
        foreach ($writers as $writer) {
            if ($this->existOnDBByImdbdID('MovieBundle:Writer', $writer['imdb'])) {
                $instanceWriter = $this->em->getRepository('MovieBundle:Writer')
                    ->findOneBy(['imdbId' => $writer['imdb']]);
            } else {
                $instanceWriter = $this->serviceWriter->newWriter($writer);
            }
            if (!$movie->checkWriterAdded($instanceWriter)) {
                $movie->addWriter($instanceWriter);
                $instanceWriter->addFilm($movie);
                $this->em->persist($instanceWriter);
                $this->em->flush();
            }

        }

        $instanceWriter = null;
    }

    public function CalculateScoreByImdbID(Movie $movie)
    {
        $rottenTomatoesRating = 50;
        $metacriticRating = 50;
        $imdbRating = 5;
        $Omdbmovie = json_decode(
            file_get_contents('http://www.omdbapi.com/?apikey=ad6d5fb5&i='.$movie->getImdbId()),
            true
        );

        if (isset($Omdbmovie['Ratings'])) {
            foreach ($Omdbmovie['Ratings'] as $rating) {
                if ($rating['Source'] == 'Internet Movie Database') {
                    $imdbRating = floatval(str_replace('/10', '', $rating['Value']));
                }
                if ($rating['Source'] == 'Rotten Tomatoes') {
                    $rottenTomatoesRating = floatval(str_replace('%', '', $rating['Value']));
                }
                if ($rating['Source'] == 'Metacritic') {
                    $metacriticRating = floatval(str_replace('/100', '', $rating['Value']));
                }
            }
        }
        if (!isset($metacriticRating)) {
            $metacriticRating = isset($Omdbmovie['Metascore']) ? $Omdbmovie['Metascore'] : 50;
        }
        $imdbVotes = isset($Omdbmovie['imdbVotes'])
            ? intval(str_replace(',', '', $Omdbmovie['imdbVotes'])) : 0;
        $movie->recalculateRating($imdbRating, $metacriticRating, $rottenTomatoesRating, 5, $imdbVotes);
        $this->em->persist($movie);
        $this->em->flush();
    }
}