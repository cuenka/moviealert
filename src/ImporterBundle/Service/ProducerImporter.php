<?php

namespace ImporterBundle\Service;

use Imdb\Config;
use Imdb\PersonSearch;
use Imdb\Title;
use Imdb\TitleSearch;
use ImporterBundle\Entity\Search;
use MovieBundle\Entity\Producer;
use MovieBundle\Entity\Movie;


/**
 * Class MovieImporter
 * @package ImporterBundle\Service
 */
class ProducerImporter extends Importer
{

    /**
     * @return mixed
     */
    public function getProducers()
    {
        return $this->em->getRepository('ImporterBundle:Search')->getProducersFromSearches();
    }

    /**
     * @param $info
     * @return Producer
     */
    public function newProducer($potentialProducer)
    {
        $producer = new Producer();
        $person = $this->getPersonBySearch($potentialProducer['name'], $potentialProducer['imdb'], Search::CONTEXT_PRODUCER);
        $producer->setSlug($this->slugger($person->name(), 'MovieBundle:Producer'))
            ->setName($person->name())
            ->setImdbId($person->imdbid());
        $biography = (isset($person->bio()[0]['desc']) ? $person->bio()[0]['desc'] : null);
        $biography = $this->cleanUpText($biography);
        $producer->setBiography($biography);
        $producer->setAge($this->normaliseDate($person->born()));
        $this->em->persist($producer);
        $this->em->flush();
        if ($person->savephoto(
            $this->imagePath. DIRECTORY_SEPARATOR. Search::CONTEXT_PRODUCER. DIRECTORY_SEPARATOR. $producer->getId(). '.jpg',
            false,
            true)) {
            $producer->setThumbnail(true);
            $this->em->persist($producer);
            $this->em->flush();
        }
        unset($person);
        unset($potentialProducer);

        return $producer;
    }

    public function importSingleProducer(Search $search, $scanFilmography = false)
    {

        $personSearch = new PersonSearch($this->config);
        $results = $personSearch-search($search->getSearch());


        foreach($results as $result) {
            if (count($result->movies_producer()) > 0) {
                    $isOnDB = $this->em->getRepository("MovieBundle:Producer")->findByIMDBId(
                        $result->imdbid()
                    );

                    if ($isOnDB == 0) {
                        $producer = new Producer();
                        $producer->setImdbId($result->imdbid());
                        $producer->setPublished(new \DateTime());
                        $producer->setModified(new \DateTime());
                        $producer->setBirthday($this->normaliseDate($result->born()));
                        $producer->setName($result->name());
                        $biography = (isset($result->bio()[0]['desc']) ? $result->bio()[0]['desc'] : null);
                        $biography = $this->cleanUpText($biography);
                        $producer->setBiography($biography);
                        $result->born();
                        $slug = $this->slugger($producer->getName(), 'MovieBundle:Producer', isset($result->born()['year'])? $result->born()['year']: 1870);
                        $producer->setSlug($slug);
                        $this->em->persist($producer);
                        $this->em->flush();
                        if ($result->savephoto(
                            $this->imagePath.'/'.Search::CONTEXT_DIRECTOR.$producer->getId().'.jpg',
                            false,
                            true)) {
                            $producer->setThumbnail(true);
                            $this->em->persist($producer);
                            $this->em->flush();
                        }

                    }

                    if ($scanFilmography == true) {
                        $potentialFilms = $result->movies_all();

                        foreach ($potentialFilms as $potentialFilm) {
                            if ($potentialFilm['title_type'] == 'Movie') {
                                $newPotentialFilm = new Search();
                                $newPotentialFilm->setContext(Search::CONTEXT_MOVIE)
                                    ->setPublished(new \DateTime())
                                    ->setUpdated(new \DateTime())
                                    ->setProcessed(false)
                                    ->setSearch($potentialFilm['name']);
                                $this->em->persist($newPotentialFilm);
                                $this->em->flush();
                            }
                        }
                }
//                }
            }
        }
    }

    public function importSinglePerson(Search $search)
    {

        $personSearch = new PersonSearch($this->config);
        $results = $personSearch->search($search->getSearch());

        foreach($results as $result) {
            if ($result->movies_producer()) {
                $isOnDB = $this->em->getRepository("MovieBundle:Producer")->findByIMDBId($result->imdbid());
                if ($isOnDB == 0) {

                }
            }
        }
    }

}