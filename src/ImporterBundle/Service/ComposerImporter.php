<?php

namespace ImporterBundle\Service;

use Imdb\Config;
use Imdb\PersonSearch;
use Imdb\Title;
use Imdb\TitleSearch;
use ImporterBundle\Api\ImdbApi;
use ImporterBundle\Entity\Search;
use MovieBundle\Entity\Composer;
use MovieBundle\Entity\Director;
use MovieBundle\Entity\Movie;
use MovieBundle\Repository\ComposerRepository;


/**
 * Class MovieImporter
 * @package ImporterBundle\Service
 */
class ComposerImporter extends Importer
{

    public function getComposers()
    {
        return $this->em->getRepository('ImporterBundle:Search')->getComposersFromSearches();
    }

    /**
     * @return mixed
     */
    public function getComposersForReview()
    {
        return $this->em->getRepository(ComposerRepository::NAME)->findForReview();
    }

    /**
     * @param $info
     * @return Director
     */
    public function newComposer($potentialComposer)
    {
        $composer = new Composer();
        $person = $this->getPersonBySearch($potentialComposer['name'], $potentialComposer['imdb'], Search::CONTEXT_COMPOSER);
        $composer->setSlug($this->slugger($person->name(), 'MovieBundle:Composer'))
            ->setName($person->name())
            ->setImdbId($person->imdbid());
        $biography = (isset($person->bio()[0]['desc']) ? $person->bio()[0]['desc'] : null);
        $biography = $this->cleanUpText($biography);
        $composer->setBiography($biography);
        $composer->setBirthday($this->normaliseDate($person->born()));
        $this->em->persist($composer);
        $this->em->flush();
        if ($person->savephoto(
            $this->imagePath. DIRECTORY_SEPARATOR. Search::CONTEXT_COMPOSER. DIRECTORY_SEPARATOR .$composer->getId(). '.jpg',
            false,
            true)) {
            $composer->setThumbnail(true);
            $this->em->persist($composer);
            $this->em->flush();
        }
        return $composer;
    }

    public function importSingleComposer(Search $search, $scanFilmography = false)
    {
        // IMDB API
        $personSearch = new PersonSearch($this->config);
        $results = $personSearch->search($search->getSearch());


        foreach($results as $result) {
            if (count($result->movies_soundtrack()) > 0) {
                    $isOnDB = $this->em->getRepository("MovieBundle:Composer")->findByIMDBId(
                        $result->imdbid()
                    );

                    if ($isOnDB == 0) {
                        $composer = new Composer();
                        $composer->setImdbId($result->imdbid());
                        $composer->setBirthday($this->normaliseDate($result->born()));
                        $composer->setName($result->name());
                        $biography = (isset($result->bio()[0]['desc']) ? $result->bio()[0]['desc'] : null);
                        $biography = $this->cleanUpText($biography);
                        $composer->setBiography($biography);
                        $slug = $this->slugger($composer->getName(), 'MovieBundle:Composer', isset($result->born()['year'])? $result->born()['year']: 1870);
                        $composer->setSlug($slug);
                        $this->em->persist($composer);
                        $this->em->flush();
                        if ($result->savephoto(
                            $this->imagePath.'/'.Search::CONTEXT_COMPOSER.DIRECTORY_SEPARATOR.$composer->getId().'.jpg',
                            false,
                            true)) {
                            $composer->setThumbnail(true);
                            $this->em->persist($composer);
                            $this->em->flush();
                        }

                    }

                    if ($scanFilmography == true) {
                        $potentialFilms = $result->movies_soundtrack();

                        foreach ($potentialFilms as $potentialFilm) {
                            if ($potentialFilm['title_type'] == 'Movie') {
                                $newPotentialFilm = new Search();
                                $newPotentialFilm->setContext(Search::CONTEXT_MOVIE)
                                    ->setPublished(new \DateTime())
                                    ->setUpdated(new \DateTime())
                                    ->setProcessed(false)
                                    ->setSearch($potentialFilm['name']);
                                $this->em->persist($newPotentialFilm);
                                $this->em->flush();
                            }
                        }
                }
//                }
            }
        }
    }

    public function importSinglePerson(Search $search)
    {

        $personSearch = new PersonSearch($this->config);
        $results = $personSearch->search($search->getSearch());

        foreach($results as $result) {
            if ($result->movies_director()) {
                $isOnDB = $this->em->getRepository("MovieBundle:Director")->findByIMDBId($result->imdbid());
                if ($isOnDB == 0) {

                }
            }
        }
    }

    /**
     * @param Composer $composer
     */
    public function reviewInstance(Composer $composer)
    {
        $imdb = new ImdbApi();
        $person = $imdb->searchOnePersonByName($composer->getName(), $composer->getImdbId());

        if(is_null($composer->getBiography())) {
            $composer->setBiography($person['biography']);
        }

        if(is_null($composer->getBirthday())) {
            $composer->setBirthday($this->normaliseDate($person['birthday']));
        }

        if (!is_null($person['thumbnail']) && ($composer->getThumbnail() === false)) {
            $imagePath =  $this->imagePath . DIRECTORY_SEPARATOR. 'composer'. DIRECTORY_SEPARATOR. $composer->getId(). '.jpg';
            $isSaved = $this->saveImage($person['thumbnail'], $imagePath);
            if ($isSaved) {
                $composer->setThumbnail(true);
            }
        }

        if (!empty($person['filmography'])) {
            foreach ($person['filmography'] as $movie) {
                $movieImdbId = 'tt'. $movie['mid'];
                $movieName = empty($movie['name']) ? null : $movie['name'];
                $movieYear = empty(intval($movie['year'])) ? null : intval($movie['year']);
                $this->createNewSearch($movieName, Search::CONTEXT_MOVIE);
                $this->em->flush();
            }
        }

        $composer->setNeedReview(false);
        $this->em->persist($composer);
        $this->em->flush();
        return 'Updated: '. $composer->getName();
    }

}