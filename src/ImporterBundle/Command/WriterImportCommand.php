<?php

namespace ImporterBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class WriterImportCommand
 * @package ImporterBundle\Command
 */
class WriterImportCommand extends ContainerAwareCommand
{
    /**
     *
     */
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('importer:writers')
            // the short description shown while running "php bin/console list"
            ->setDescription('Import writers from recent searches')
            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('Import writers from recent searches...')
            ->addOption(
                'scan-films',
                null,
                InputOption::VALUE_REQUIRED,
                'Do you wish to scan filmography? (yes or no)',
                'no'
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // outputs multiple lines to the console (adding "\n" at the end of each line)
        $output->writeln([
            'Importer Started',
            '================',
            ''
        ]);
        $importerService = $this->getContainer()->get("importer.writer");

        $writers = $importerService->getWriters();
        $scanFilms = ($input->getOption('scan-films') == 'yes' ? true : false);
        foreach($writers as $writers) {
            $output->writeln('Importing: '. $writers->getSearch());
            $importerService->importSingleWriter($writers->getSearch(), $scanFilms);
            $importerService->updateStatusSearch($writers);
        }
        $output->writeln('All done');
    }
}