<?php

namespace ImporterBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class DirectorImportCommand
 * @package ImporterBundle\Command
 */
class DirectorImportCommand extends ContainerAwareCommand
{
    /**
     *
     */
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('importer:directors')
            // the short description shown while running "php bin/console list"
            ->setDescription('Import directors from recent searches')
            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('Import directors from recent searches...')
            ->addOption(
                'scan-films',
                null,
                InputOption::VALUE_REQUIRED,
                'Do you wish to scan filmography? (yes or no)',
                'no'
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // outputs multiple lines to the console (adding "\n" at the end of each line)
        $output->writeln([
            'Importer Started',
            '================',
            ''
        ]);
        $importerService = $this->getContainer()->get("importer.director");

        $directors = $importerService->getDirectors();
        $scanFilms = ($input->getOption('scan-films') == 'yes' ? true : false);
        foreach($directors as $director) {
            $output->writeln('Importing: '. $director->getSearch());
            $importerService->importSingleDirector($director, $scanFilms);
            $importerService->updateStatusSearch($director);
        }
        $output->writeln('All done');
    }
}