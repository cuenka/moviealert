<?php

namespace ImporterBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class MovieImportCommand
 * @package ImporterBundle\Command
 */
class MovieImportCommand extends ContainerAwareCommand
{
    /**
     *
     */
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('importer:movies')
            // the short description shown while running "php bin/console list"
            ->setDescription('Import movies from recent searches')
            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('Import movies from recent searches...');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            // outputs multiple lines to the console (adding "\n" at the end of each line)
            $output->writeln(
                [
                    'Importer Started',
                    '============',
                    '',
                ]
            );
            $importerService = $this->getContainer()->get("importer.movie");

            $movies = $importerService->getMovies();

            foreach ($movies as $movie) {
                $output->writeln('Importing: '.$movie->getSearch());
                $importerService->importSingleMovieBySearch($movie);
                $importerService->updateStatusSearch($movie);
            }
            $output->writeln('All done');
        } catch (\Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }
}