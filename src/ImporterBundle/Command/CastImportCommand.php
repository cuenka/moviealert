<?php

namespace ImporterBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CastImportCommand
 * @package ImporterBundle\Command
 */
class CastImportCommand extends ContainerAwareCommand
{
    /**
     *
     */
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('importer:casts')
            // the short description shown while running "php bin/console list"
            ->setDescription('Import casts from recent searches')
            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('Import casts from recent searches...')
            ->addOption(
                'scan-films',
                null,
                InputOption::VALUE_REQUIRED,
                'Do you wish to scan filmography? (yes or no)',
                'no'
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // outputs multiple lines to the console (adding "\n" at the end of each line)
        $output->writeln([
            'Importer Started',
            '================',
            ''
        ]);
        $importerService = $this->getContainer()->get("importer.cast");

        $casts = $importerService->getCast();
        $scanFilms = ($input->getOption('scan-films') == 'yes' ? true : false);
        foreach($casts as $key => $cast) {
            $output->writeln($key. ': '. $cast->getSearch());
            clearstatcache();
            $output->writeln('Importing: '. $cast->getSearch());
            $importerService->importSingleCast($cast, $scanFilms);
            $importerService->updateStatusSearch($cast);
        }
        $output->writeln('All done');
    }
}