<?php

namespace ImporterBundle\Command;

use ImporterBundle\Entity\Search;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CastImportCommand
 * @package ImporterBundle\Command
 */
class ReviewImportCommand extends ContainerAwareCommand
{
    /**
     *
     */
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('importer:review')
            // the short description shown while running "php bin/console list"
            ->setDescription('Import casts from recent searches')
            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('Import casts from recent searches...')
            ->addArgument(
                'context',
                InputArgument::OPTIONAL,
                'context, same than search contexts',
                Search::CONTEXT_MOVIE
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // outputs multiple lines to the console (adding "\n" at the end of each line)
        $output->writeln([
            'Importer Review Started',
            '=======================',
            ''
        ]);

        switch ($input->getArgument('context')) {
            case Search::CONTEXT_MOVIE:
                $importerService = $this->getContainer()->get('importer.movie');
                $items = $importerService->getMoviesForReview();
                break;
            case Search::CONTEXT_CAST:
                $importerService = $this->getContainer()->get('importer.cast');
                $items = $importerService->getCastForReview();
                break;
            case Search::CONTEXT_COMPOSER:
                $importerService = $this->getContainer()->get('importer.composer');
                $items = $importerService->getComposersForReview();
                break;
            case Search::CONTEXT_DIRECTOR:
                $importerService = $this->getContainer()->get('importer.director');
                $items = $importerService->getComposersForReview();
                break;
            case Search::CONTEXT_PRODUCER:
                break;
            case Search::CONTEXT_WRITER:
                $importerService = $this->getContainer()->get('importer.writer');
                $items = $importerService->getWritersForReview();
                break;
        }

        foreach($items as $key => $item) {
            $output->writeln($key. ': '. $item->getId());
            $status = $importerService->reviewInstance($item);
            $output->writeln('===> '. $status);
            sleep(10);
        }
        $output->writeln('All done');
    }
}