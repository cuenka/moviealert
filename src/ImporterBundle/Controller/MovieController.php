<?php

namespace ImporterBundle\Controller;

use Imdb\Config;
use Imdb\Title;
use Imdb\TitleSearch;
use ImporterBundle\Entity\Search;
use MovieBundle\Entity\Movie;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use ImporterBundle\Form\SearchType;

class MovieController extends Controller
{
    /**
     * @Route("/movie", name="importer_movie")
     */
    public function movieAction()
    {
        $config = new Config();
        $config->language = 'en-US';
        $service = $this->get("importer.movie");
        $em = $this->getDoctrine()->getManager();
        $imdb = new Title(335266, $config);
        $imdbsearch = new TitleSearch();

        $results = $imdbsearch->search('todo sobre mi madre', array(TitleSearch::MOVIE));
        foreach($results as $result) {
            $isOnDB = $em->getRepository("MovieBundle:Movie")->findByIMDBId($result->imdbid());
            if ($isOnDB == 0) {
                $movie = new Movie();
                $imdbMovie = new Title($result->imdbid(), $config);
                $movie->setFromIMDB($imdbMovie);
                $slug = $service->slugger($movie->getTitle(),'MovieBundle:Movie',$movie->getYear());
                $movie->setSlug($slug);
                $countries = $em->getRepository("MovieBundle:Country")->findByName($imdbMovie->country());
                if (count($countries) < count($imdbMovie->country())) {
                    $em->getRepository("MovieBundle:Country")->FindOrAddCountries($imdbMovie->country());
                }

                $em->persist($movie);
                $em->flush();
            }
        }
        return $this->render('ImporterBundle:Default:index.html.twig');
    }

    /**
     * @Route("/search/{type}", name="importer_movie")
     * @param Request $request
     * @param $type
     */
    public function searchAction(Request $request, $type)
    {
        $em = $this->getDoctrine()->getManager();
        $searchTypesAvaiables = $this->getParameter('search_types');
        if (!in_array($type, $searchTypesAvaiables)) {
            return $this->render('ImporterBundle:Default:index.html.twig');
        }

        $search = new Search();
        $searchForm = $this->createForm(SearchType::class, $search, ['data_context'=>$type]);
        $searchForm->handleRequest($request);

        if ($searchForm->isSubmitted() && $searchForm->isValid()) {
            $search->setPublished(new \DateTime());
            $search->setUpdated(new \DateTime());
            $em->persist($search);
            $em->flush();
            return $this->redirectToRoute('importer_movie', ['type' => $type]);
        }

        $searches = $em->getRepository("ImporterBundle:Search")->getInfoForTable();
        $actionNames =
            [
                'backend_movieedit',
                'backend_moviedelete'
            ];
        return $this->render('ImporterBundle:Default:search.html.twig', array(
            'data' => $searches,
            'actions' => $actionNames,
            'form' => $searchForm->createView()
        ));

    }
}
