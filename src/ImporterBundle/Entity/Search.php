<?php

namespace ImporterBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Search
 *
 * @ORM\Table(name="searches", indexes={
 * @ORM\Index(name="id_idx", columns={"id"}),
 * @ORM\Index(name="search_idx", columns={"search"})
 * })
 * @ORM\Entity(repositoryClass="ImporterBundle\Repository\SearchRepository")
 */
class Search
{
    const CONTEXT_CAST = 'cast';
    const CONTEXT_COMPOSER = 'composer';
    const CONTEXT_DIRECTOR = 'director';
    const CONTEXT_MOVIE = 'movie';
    const CONTEXT_PRODUCER = 'producer';
    const CONTEXT_WRITER = 'writer';
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="published", type="datetime")
     */
    private $published;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;

    /**
     * @var string
     *
     * @ORM\Column(name="search", type="string", length=255)
     */
    private $search;

    /**
     * @var string
     *
     * @ORM\Column(name="context", type="string", length=255)
     */
    private $context;

    /**
     * @var string
     *
     * @ORM\Column(name="processed", type="boolean")
     */
    private $processed = false;

    /**
     * @var string
     *
     * @ORM\Column(name="imdbId", type="string", length=50, unique=false, nullable=true )
     */
    private $imdbId;

    /**
     * Search constructor.
     */
    public function __construct()
    {
        $this->published = new \DateTime();
        $this->updated = new \DateTime();
        $this->imdbId = null;
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * @param \DateTime $published
     */
    public function setPublished($published)
    {
        $this->published = $published;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * @return string
     */
    public function getSearch()
    {
        return $this->search;
    }

    /**
     * @param string $search
     */
    public function setSearch($search)
    {
        $this->search = $search;

        return $this;
    }

    /**
     * @return string
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * @param string $context
     */
    public function setContext($context)
    {
        $this->context = $context;

        return $this;
    }

    /**
     * @return string
     */
    public function getProcessed()
    {
        return $this->processed;
    }

    /**
     * @param boolean $processed
     */
    public function setProcessed($processed)
    {
        $this->processed = $processed;

        return $this;
    }

    /**
     * @return string
     */
    public function getImdbId()
    {
        return $this->imdbId;
    }

    /**
     * @param string $imdbId
     */
    public function setImdbId($imdbId)
    {
        $this->imdbId = $imdbId;
    }


}

