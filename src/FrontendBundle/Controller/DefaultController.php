<?php

namespace FrontendBundle\Controller;

use ImporterBundle\Api\ImdbApi;
use ImporterBundle\Api\OmdbApi;
use ImporterBundle\Api\TmdbApi;
use ImporterBundle\Entity\Search;
use ImporterBundle\Form\SearchType;
use ImporterBundle\Service\MovieImporter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="frontend_index")
     */
    public function indexAction(Request $request)
    {
        return $this->render('FrontendBundle:Default:index.html.twig',
            [
                'page' => 'homepage'
            ]);
    }

    /**
     * @Route("/cast", name="frontend_cast")
     */
    public function castAction(Request $request)
    {
        $type = Search::CONTEXT_CAST;

        $em = $this->getDoctrine()->getManager();
        $searchTypesAvaiables = $this->getParameter('search_types');
        if (!in_array($type, $searchTypesAvaiables)) {
            return $this->redirectToRoute('frontend_index');
        }

        $search = new Search();
        $searchForm = $this->createForm(SearchType::class, $search, ['data_context'=>$type]);
        $searchForm->handleRequest($request);
        if ($searchForm->isSubmitted() && $searchForm->isValid()) {
            $search->setPublished(new \DateTime());
            $search->setUpdated(new \DateTime());
            $em->persist($search);
            $em->flush();
            return $this->redirectToRoute('frontend_index');
        }

        return $this->render('FrontendBundle:Default:cast.html.twig', array(
            'form' => $searchForm->createView()
        ));
    }

    /**
     * @Route("/directors", name="frontend_directors")
     */
    public function directorsAction(Request $request)
    {
        $type = Search::CONTEXT_DIRECTOR;

        $em = $this->getDoctrine()->getManager();
        $searchTypesAvaiables = $this->getParameter('search_types');
        if (!in_array($type, $searchTypesAvaiables)) {
            return $this->redirectToRoute('frontend_index');
        }

        $search = new Search();
        $searchForm = $this->createForm(SearchType::class, $search, ['data_context'=>$type]);
        $searchForm->handleRequest($request);
        if ($searchForm->isSubmitted() && $searchForm->isValid()) {
            $search->setPublished(new \DateTime());
            $search->setUpdated(new \DateTime());
            $em->persist($search);
            $em->flush();
            return $this->redirectToRoute('frontend_index');
        }

        return $this->render('FrontendBundle:Default:directors.twig', array(
            'form' => $searchForm->createView()
        ));
    }

    /**
     * @Route("/composers", name="frontend_composers")
     */
    public function composersAction(Request $request)
    {
        $type = Search::CONTEXT_COMPOSER;

        $em = $this->getDoctrine()->getManager();
        $searchTypesAvaiables = $this->getParameter('search_types');
        if (!in_array($type, $searchTypesAvaiables)) {
            return $this->redirectToRoute('frontend_index');
        }

        $search = new Search();
        $searchForm = $this->createForm(SearchType::class, $search, ['data_context'=>$type]);
        $searchForm->handleRequest($request);
        if ($searchForm->isSubmitted() && $searchForm->isValid()) {
            $search->setPublished(new \DateTime());
            $search->setUpdated(new \DateTime());
            $em->persist($search);
            $em->flush();
            return $this->redirectToRoute('frontend_index');
        }

        return $this->render('FrontendBundle:Default:composers.html.twig', array(
            'form' => $searchForm->createView()
        ));
    }

    /**
     * @Route("/writers", name="frontend_writers")
     */
    public function writersAction(Request $request)
    {
        $type = Search::CONTEXT_WRITER;

        $em = $this->getDoctrine()->getManager();
        $searchTypesAvaiables = $this->getParameter('search_types');
        if (!in_array($type, $searchTypesAvaiables)) {
            return $this->redirectToRoute('frontend_index');
        }

        $search = new Search();
        $searchForm = $this->createForm(SearchType::class, $search, ['data_context'=>$type]);
        $searchForm->handleRequest($request);
        if ($searchForm->isSubmitted() && $searchForm->isValid()) {
            $search->setPublished(new \DateTime());
            $search->setUpdated(new \DateTime());
            $em->persist($search);
            $em->flush();
            return $this->redirectToRoute('frontend_index');
        }

        return $this->render('FrontendBundle:Default:writers.twig', array(
            'form' => $searchForm->createView()
        ));
    }

    /**
     * @Route("/producers", name="frontend_producers")
     */
    public function producersAction(Request $request)
    {
        $type = Search::CONTEXT_PRODUCER;

        $em = $this->getDoctrine()->getManager();
        $searchTypesAvaiables = $this->getParameter('search_types');
        if (!in_array($type, $searchTypesAvaiables)) {
            return $this->redirectToRoute('frontend_index');
        }

        $search = new Search();
        $searchForm = $this->createForm(SearchType::class, $search, ['data_context'=>$type]);
        $searchForm->handleRequest($request);
        if ($searchForm->isSubmitted() && $searchForm->isValid()) {
            $search->setPublished(new \DateTime());
            $search->setUpdated(new \DateTime());
            $em->persist($search);
            $em->flush();
            return $this->redirectToRoute('frontend_index');
        }

        return $this->render('FrontendBundle:Default:producers.html.twig', array(
            'form' => $searchForm->createView()
        ));
    }


}
