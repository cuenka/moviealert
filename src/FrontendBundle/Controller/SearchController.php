<?php

namespace FrontendBundle\Controller;

use ImporterBundle\Entity\Search;
use ImporterBundle\Form\SearchType;
use MovieBundle\Entity\Movie;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class SearchController extends Controller
{
    /**
     * @Route("/search", name="frontend_search")
     */
    public function searchAction(Request $request)
    {
        $data = null;

        $em = $this->getDoctrine()->getManager();
        $searchTypesAvailable = $this->getParameter('search_types');

        $search = new Search();
        $searchForm = $this->createForm(SearchType::class, $search,
            [
                'searchTypesAvailable' => $searchTypesAvailable,
                'action' => $this->generateUrl('frontend_search'),
                'method' => 'POST'
            ]);
        $searchForm->handleRequest($request);
        if ($searchForm->isSubmitted() && $searchForm->isValid()) {
            $data = $this->search($request->request->all());
            if (empty($data['results'])) {
                $search
                    ->setContext($data['context'])
                    ->setSearch($data['search']);
                $em->persist($search);
                $em->flush();
            }
        }

        return $this->render('FrontendBundle:Search:search.html.twig', array(
            'page' => 'p-search',
            'form' => $searchForm->createView(),
            'data' => $data,
            'imagePath' => $data['imagePath'] ?  $data['imagePath'] : null
        ));
    }

    /**
     * @Route("/search", name="frontend_search-template")
     */
    public function searchTemplateAction(Request $request)
    {
       $searchTypesAvailable = $this->getParameter('search_types');
         $search = new Search();
        $searchForm = $this->createForm(SearchType::class, $search,
            [
                'searchTypesAvailable' => $searchTypesAvailable,
                'action' => $this->generateUrl('frontend_search'),
                'method' => 'POST'
            ]);
        $searchForm->handleRequest($request);

        return $this->render('FrontendBundle:Search:_search-form.html.twig', array(
            'form' => $searchForm->createView()
        ));
    }


    private function search($request)
    {
        $search = $request['search'];
        switch($search['context']) {
            case Search::CONTEXT_DIRECTOR:
                break;
            case Search::CONTEXT_CAST:
                break;
            case Search::CONTEXT_COMPOSER:
                break;
            case Search::CONTEXT_PRODUCER:
                break;
            case Search::CONTEXT_WRITER:
                $data['results'] = $this->getDoctrine()->getRepository('MovieBundle:Writer')->searchBy($search['search']);
                $data['imagePath'] = '/images/movie/';
                break;
            case Search::CONTEXT_MOVIE:
            default:
            $data['results'] = $this->getDoctrine()->getRepository('MovieBundle:Movie')->searchBy($search['search']);
            $data['imagePath'] = '/images/movie/';
                break;
        }
        $data['context'] = $search['context'];
        $data['search'] = $search['search'];
        return $data;

    }
}
