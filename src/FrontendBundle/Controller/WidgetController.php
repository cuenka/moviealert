<?php

namespace FrontendBundle\Controller;

use ImporterBundle\Entity\Search;
use ImporterBundle\Form\SearchType;
use MovieBundle\Entity\Movie;
use MovieBundle\Repository\CastRepository;
use MovieBundle\Repository\MovieRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class WidgetController
 * @package FrontendBundle\Controller
 */
class WidgetController extends Controller
{
    /**
     * @Route("/top-release-month/{month", defaults={"month"=0}, name="frontend_widget-top-release-month")
     */
    public function topReleaseMonthAction(Request $request, $month)
    {
        $fromDate =($month == 0) ? new \DateTime('first day of this month') : new \DateTime("Y-$month-01");
        $lastDate =($month == 0) ? new \DateTime('last day of this month') : new \DateTime("last day of $month-Y");
        $movies = $this->getDoctrine()
            ->getRepository('MovieBundle:Movie')
            ->findTopByRangeDate($fromDate, $lastDate);

        return $this->render(
            'FrontendBundle:Widget:topMovies.html.twig',
            array(
                'movies' => $movies,
                'title' => 'Releases of the Month',
                'imagePath' => '/images/movie/',
                'date' => true
            )
        );
    }

    /**
     * @Route("/top-movies", name="frontend_topmovies")
     */
    public function topMoviesAction(Request $request)
    {
        $movies = $this->getDoctrine()
            ->getRepository('MovieBundle:Movie')
            ->getTopMovies(10);

        return $this->render(
            'FrontendBundle:Widget:topMovies.html.twig',
            array(
                'movies' => $movies,
                'title' => 'Top Movies',
                'imagePath' => '/images/movie/',
            )
        );
    }

    /**
     * @Route("/top-composers", name="frontend_topcomposers")
     */
    public function topComposersAction(Request $request)
    {
        $composers = $this->getDoctrine()
            ->getRepository('MovieBundle:Composer')
            ->getTopComposers(10);

        return $this->render(
            'FrontendBundle:Widget:topComposers.html.twig',
            array(
                'composers' => $composers,
                'imagePath' => '/images/composers/',
            )
        );
    }

    /**
     * @Route("/novelty-of-the-month", name="frontend_noveltymonth")
     */
    public function noveltyOfTheMonthAction(Request $request)
    {
        $movieId = $this->getParameter('novelty_of_month');
        $movie = $this->getDoctrine()
            ->getRepository(MovieRepository::NAME)
            ->findOneBy(['id' => $movieId]);

        return $this->render(
            'FrontendBundle:Widget:MovieOfMonth.html.twig',
            array(
                'title' => 'People are talking about...',
                'movie' => $movie,
                'imagePath' => '/images/movie/',
            )
        );
    }

    /**
     * @Route("/movie-of-the-month", name="frontend_moviemonth")
     */
    public function movieOfTheMonthAction(Request $request)
    {
        $movieId = $this->getParameter('movie_of_month');
        $movie = $this->getDoctrine()
            ->getRepository(MovieRepository::NAME)
            ->findOneBy(['id' => $movieId]);

        return $this->render(
            'FrontendBundle:Widget:MovieOfMonth.html.twig',
            array(
                'title' => 'Movie of the month',
                'movie' => $movie,
                'imagePath' => '/images/movie/',
            )
        );
    }

    /**
     * @Route("/top-cast", name="topCast")
     */
    public function topCastAction(Request $request)
    {
        $cast = $this->getDoctrine()->getRepository(CastRepository::NAME)
            ->getTopCast();

        return $this->render(
            'FrontendBundle:Widget:topCast.html.twig',
            [
                'casts' => $cast,
                'imagePath' => '/images/cast/',
            ]
        );
    }
}
